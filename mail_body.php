<?php

    // 管理者用メール本文
    function makeMailBodyAdmin($arrSend_value) {

        $ah_list_surv = '';
        for ($i=1; $i < 7; $i++) {
            if (isset($arrSend_value["AH$i"])){
                $ah_list_surv .= $arrSend_value["AH$i"] .",";
            }
        }
        $ah_list_surv = rtrim($ah_list_surv, ',');

        $bh_list_surv = '';
        for ($i=1; $i < 6; $i++) {
            if (isset($arrSend_value["BH$i"])){
                $bh_list_surv .= $arrSend_value["BH$i"] .",";
            }
        }
        $bh_list_surv = rtrim($bh_list_surv, ',');

        $ch_list_surv = '';
        for ($i=1; $i < 10; $i++) {
            if (isset($arrSend_value["CH$i"])){
                $ch_list_surv .= $arrSend_value["CH$i"] .",";
            }
        }
        $ch_list_surv = rtrim($ch_list_surv, ',');

        $dh_list_surv = '';
        for ($i=1; $i < 8; $i++) {
            if (isset($arrSend_value["DH$i"])){
                $dh_list_surv .= $arrSend_value["DH$i"] .",";
            }
        }
        $dh_list_surv = rtrim($dh_list_surv, ',');

        $eh_list_surv = '';
        for ($i=1; $i < 6; $i++) {
            if (isset($arrSend_value["EH$i"])){
                $eh_list_surv .= $arrSend_value["EH$i"] .",";
            }
        }
        $eh_list_surv = rtrim($eh_list_surv, ',');

        $proposal_number ='';
        if (isset($arrSend_value['proposal_content'])) {
            switch ($arrSend_value['proposal_content']) {
                case '満足':
                    $proposal_number = '5';
                    break;
                case 'ほぼ満足':
                    $proposal_number = '4';
                    break;
                case '普通':
                    $proposal_number = '3';
                    break;
                case 'やや不満':
                    $proposal_number = '2';
                    break;
                case '不満':
                    $proposal_number = '1';
                    break;
            }
        }

        $Personin_number ='';
        if (isset($arrSend_value['Personin_charge'])) {
            switch ($arrSend_value['Personin_charge']) {
                case '満足':
                    $Personin_number = '5';
                    break;
                case 'ほぼ満足':
                    $Personin_number = '4';
                    break;
                case '普通':
                    $Personin_number = '3';
                    break;
                case 'やや不満':
                    $Personin_number = '2';
                    break;
                case '不満':
                    $Personin_number = '1';
                    break;
            }
        }

        $achievement_number ='';
        if (isset($arrSend_value['achievement'])) {
            switch ($arrSend_value['achievement']) {
                case '満足':
                    $achievement_number = '5';
                    break;
                case 'ほぼ満足':
                    $achievement_number = '4';
                    break;
                case '普通':
                    $achievement_number = '3';
                    break;
                case 'やや不満':
                    $achievement_number = '2';
                    break;
                case '不満':
                    $achievement_number = '1';
                    break;
            }
        }

        $worker_number ='';
        if (isset($arrSend_value['worker_response'])) {
            switch ($arrSend_value['worker_response']) {
                case '満足':
                    $worker_number = '5';
                    break;
                case 'ほぼ満足':
                    $worker_number = '4';
                    break;
                case '普通':
                    $worker_number = '3';
                    break;
                case 'やや不満':
                    $worker_number = '2';
                    break;
                case '不満':
                    $worker_number = '1';
                    break;
            }
        }

        $due_date_number ='';
        if (isset($arrSend_value['due_date'])) {
            switch ($arrSend_value['due_date']) {
                case '満足':
                    $due_date_number = '5';
                    break;
                case 'ほぼ満足':
                    $due_date_number = '4';
                    break;
                case '普通':
                    $due_date_number = '3';
                    break;
                case 'やや不満':
                    $due_date_number = '2';
                    break;
                case '不満':
                    $due_date_number = '1';
                    break;
            }
        }

        $evaluation_number ='';
        if (isset($arrSend_value['evaluation'])) {
            switch ($arrSend_value['evaluation']) {
                case '満足':
                    $evaluation_number = '5';
                    break;
                case 'ほぼ満足':
                    $evaluation_number = '4';
                    break;
                case '普通':
                    $evaluation_number = '3';
                    break;
                case 'やや不満':
                    $evaluation_number = '2';
                    break;
                case '不満':
                    $evaluation_number = '1';
                    break;
            }
        }

        

        $send_date = date("Y/m/d H:i");

        $mail_body = <<< EOM

東急Ｒｅ・デザインのマンションリフォーム紹介サイト＜https://mr.tokyu-re-design.co.jp＞経由で
下記のお客様よりお問い合わせがありました。
お客様へのご連絡宜しくお願いします。
========================================================

フォーム名称：リフォームアフタ－アンケート
登録日時　　：{$send_date}

【お名前】{$arrSend_value['name']} 様
【お名前(フリガナ)】{$arrSend_value['name_kana']} 様
【マンション名】{$arrSend_value['name_mansion']}
【お部屋番号】{$arrSend_value['room_number']}

【メールアドレス】{$arrSend_value['Mail']}

【アンケート】

【Q1.リフォーム工事に対するご意見】

【弊社の提案内容】{$arrSend_value['proposal_content']}（{$proposal_number}）
【担当者の対応】{$arrSend_value['Personin_charge']}（{$Personin_number}）
【工事の出来栄え】{$arrSend_value['achievement']}（{$achievement_number}）
【現場作業員の対応】{$arrSend_value['worker_response']}（{$worker_number}）
【施工管理／期日】{$arrSend_value['due_date']}（{$due_date_number}）
【総合評価】{$arrSend_value['evaluation']}（{$evaluation_number}）

【Q2.弊社にご発注いただいた決め手】
　{$ah_list_surv}
EOM;
if (isset($arrSend_value["AH7"]) && $arrSend_value["AH7"] == 'その他：') {
    $mail_body .= <<< EOM
,その他 : {$arrSend_value['AH7_by_others']}
EOM;
}
$mail_body .= <<< EOM


【Q3.今後リフォームを検討する際、関心がある内容】
　{$bh_list_surv}
EOM;
if (isset($arrSend_value["BH6"]) && $arrSend_value["BH6"] == 'その他：') {
    $mail_body .= <<< EOM
,その他 : {$arrSend_value['BH6_by_others']}
EOM;
}
$mail_body .= <<< EOM


【Q4.今後リフォームを検討したい部位（場所）】
　{$ch_list_surv}
EOM;
if (isset($arrSend_value["CH10"]) && $arrSend_value["CH10"] == 'その他：') {
    $mail_body .= <<< EOM
,その他 : {$arrSend_value['CH10_by_others']}
EOM;
}
$mail_body .= <<< EOM


【Q5.次回のリフォームのご予定】
　{$dh_list_surv}
EOM;
if (isset($arrSend_value["DH8"]) && $arrSend_value["DH8"] == 'その他：') {
    $mail_body .= <<< EOM
,その他 : {$arrSend_value['DH8_by_others']}
EOM;
}
$mail_body .= <<< EOM


【Q6.今後どのような情報の提供】
　{$eh_list_surv}
EOM;
if (isset($arrSend_value["EH6"]) && $arrSend_value["EH6"] == 'その他：') {
    $mail_body .= <<< EOM
,その他 : {$arrSend_value['EH6_by_others']}
EOM;
}
$mail_body .= <<< EOM


【その他お気づきの点、ご要望・ご相談】
　{$arrSend_value['Note']}

========================================================

【お客様へのサンクスメールの写し】

EOM;
        $mail_body .= makeMailBodyUser($arrSend_value);

        return $mail_body;
    }


    // 申込者用メール本文
    function makeMailBodyUser($arrSend_value) {

        $ah_list_surv = '';
        for ($i=1; $i < 7; $i++) {
            if (isset($arrSend_value["AH$i"])){
                $ah_list_surv .= $arrSend_value["AH$i"] ."\n　";
            }
        }

        $bh_list_surv = '';
        for ($i=1; $i < 6; $i++) {
            if (isset($arrSend_value["BH$i"])){
                $bh_list_surv .= $arrSend_value["BH$i"] ."\n　";
            }
        }

        $ch_list_surv = '';
        for ($i=1; $i < 10; $i++) {
            if (isset($arrSend_value["CH$i"])){
                $ch_list_surv .= $arrSend_value["CH$i"] ."\n　";
            }
        }

        $dh_list_surv = '';
        for ($i=1; $i < 8; $i++) {
            if (isset($arrSend_value["DH$i"])){
                $dh_list_surv .= $arrSend_value["DH$i"] ."\n　";
            }
        }

        $eh_list_surv = '';
        for ($i=1; $i < 6; $i++) {
            if (isset($arrSend_value["EH$i"])){
                $eh_list_surv .= $arrSend_value["EH$i"] ."\n　";
            }
        }

        $proposal_number ='';
        if (isset($arrSend_value['proposal_content'])) {
            switch ($arrSend_value['proposal_content']) {
                case '満足':
                    $proposal_number = '5';
                    break;
                case 'ほぼ満足':
                    $proposal_number = '4';
                    break;
                case '普通':
                    $proposal_number = '3';
                    break;
                case 'やや不満':
                    $proposal_number = '2';
                    break;
                case '不満':
                    $proposal_number = '1';
                    break;
            }
        }

        $Personin_number ='';
        if (isset($arrSend_value['Personin_charge'])) {
            switch ($arrSend_value['Personin_charge']) {
                case '満足':
                    $Personin_number = '5';
                    break;
                case 'ほぼ満足':
                    $Personin_number = '4';
                    break;
                case '普通':
                    $Personin_number = '3';
                    break;
                case 'やや不満':
                    $Personin_number = '2';
                    break;
                case '不満':
                    $Personin_number = '1';
                    break;
            }
        }

        $achievement_number ='';
        if (isset($arrSend_value['achievement'])) {
            switch ($arrSend_value['achievement']) {
                case '満足':
                    $achievement_number = '5';
                    break;
                case 'ほぼ満足':
                    $achievement_number = '4';
                    break;
                case '普通':
                    $achievement_number = '3';
                    break;
                case 'やや不満':
                    $achievement_number = '2';
                    break;
                case '不満':
                    $achievement_number = '1';
                    break;
            }
        }

        $worker_number ='';
        if (isset($arrSend_value['worker_response'])) {
            switch ($arrSend_value['worker_response']) {
                case '満足':
                    $worker_number = '5';
                    break;
                case 'ほぼ満足':
                    $worker_number = '4';
                    break;
                case '普通':
                    $worker_number = '3';
                    break;
                case 'やや不満':
                    $worker_number = '2';
                    break;
                case '不満':
                    $worker_number = '1';
                    break;
            }
        }

        $due_date_number ='';
        if (isset($arrSend_value['due_date'])) {
            switch ($arrSend_value['due_date']) {
                case '満足':
                    $due_date_number = '5';
                    break;
                case 'ほぼ満足':
                    $due_date_number = '4';
                    break;
                case '普通':
                    $due_date_number = '3';
                    break;
                case 'やや不満':
                    $due_date_number = '2';
                    break;
                case '不満':
                    $due_date_number = '1';
                    break;
            }
        }

        $evaluation_number ='';
        if (isset($arrSend_value['evaluation'])) {
            switch ($arrSend_value['evaluation']) {
                case '満足':
                    $evaluation_number = '5';
                    break;
                case 'ほぼ満足':
                    $evaluation_number = '4';
                    break;
                case '普通':
                    $evaluation_number = '3';
                    break;
                case 'やや不満':
                    $evaluation_number = '2';
                    break;
                case '不満':
                    $evaluation_number = '1';
                    break;
            }
        }

        $mail_body = <<< EOM

{$arrSend_value['name']} 様

この度は、東急Ｒｅ・デザインへ、リフォームアフタ－アンケートのご連絡をいただきましてありがとうございます。

お問合せ内容につきましては、確認次第（通常は3営業日以内）ご連絡いたします。
緊急の場合や３営業日以上経っても当社担当者より連絡がない場合は、お手数ではございますが、
0120-994-109までご連絡をお願いいたします。

お客様のご入力いただいた内容は下記のとおりです。
========================================================

【お名前】{$arrSend_value['name']} 様
【お名前(フリガナ)】{$arrSend_value['name_kana']} 様
【マンション名】{$arrSend_value['name_mansion']}
【お部屋番号】{$arrSend_value['room_number']}

【メールアドレス】{$arrSend_value['Mail']}

【アンケート】

【Q1.リフォーム工事に対するご意見】

【弊社の提案内容】{$arrSend_value['proposal_content']}（{$proposal_number}）
【担当者の対応】{$arrSend_value['Personin_charge']}（{$Personin_number}）
【工事の出来栄え】{$arrSend_value['achievement']}（{$achievement_number}）
【現場作業員の対応】{$arrSend_value['worker_response']}（{$worker_number}）
【施工管理／期日】{$arrSend_value['due_date']}（{$due_date_number}）
【総合評価】{$arrSend_value['evaluation']}（{$evaluation_number}）

【Q2.弊社にご発注いただいた決め手】
　{$ah_list_surv}
EOM;
if (isset($arrSend_value["AH7"]) && $arrSend_value["AH7"] == 'その他：') {
    $mail_body .= <<< EOM
その他 : {$arrSend_value['AH7_by_others']}\r\n
EOM;
}
$mail_body .= <<< EOM

【Q3.今後リフォームを検討する際、関心がある内容】
　{$bh_list_surv}
EOM;
if (isset($arrSend_value["BH6"]) && $arrSend_value["BH6"] == 'その他：') {
    $mail_body .= <<< EOM
その他 : {$arrSend_value['BH6_by_others']}\r\n
EOM;
}
$mail_body .= <<< EOM

【Q4.今後リフォームを検討したい部位（場所）】
　{$ch_list_surv}
EOM;
if (isset($arrSend_value["CH10"]) && $arrSend_value["CH10"] == 'その他：') {
    $mail_body .= <<< EOM
その他 : {$arrSend_value['CH10_by_others']}\r\n
EOM;
}
$mail_body .= <<< EOM

【Q5.次回のリフォームのご予定】
　{$dh_list_surv}
EOM;
if (isset($arrSend_value["DH8"]) && $arrSend_value["DH8"] == 'その他：') {
    $mail_body .= <<< EOM
その他 : {$arrSend_value['DH8_by_others']}\r\n
EOM;
}
$mail_body .= <<< EOM

【Q6.今後どのような情報の提供】
　{$eh_list_surv}
EOM;
if (isset($arrSend_value["EH6"]) && $arrSend_value["EH6"] == 'その他：') {
    $mail_body .= <<< EOM
その他 : {$arrSend_value['EH6_by_others']}\r\n
EOM;
}
$mail_body .= <<< EOM

【その他お気づきの点、ご要望・ご相談】
　{$arrSend_value['Note']}

========================================================

当メールにお心当たりのない場合やご不明な点がある場合は
当メールの送信元アドレスへお問い合わせください。
お手数ですが、下記までご連絡ください。
---------------------------------------------
株式会社 東急Ｒｅ・デザイン
マンションリフォーム事業部

〒150-0034
東京都渋谷区代官山町20-23　TENOHA代官山

フリーコール：0120-994-109
（受付時間：9時～18時／年末年始を除く）

メールアドレス：trdreform@tokyu-rd.jp
---------------------------------------------
EOM;
        return $mail_body;
    }
