<?php
    // 入力項目のリストを設定
    $arrName_list = array(
                          'name','name_kana','name_mansion','room_number','Mail',
                          'AH7_by_others','BH6_by_others','CH10_by_others',
                          'DH8_by_others','EH6_by_others','Note',
                         );

    // HTML入力フォームを表示
    function displayHtmlInputForm($arrValue=array(), $arrErrorText=array() ) {

        // 入力項目がセットされているか確認して変数に設定（PHP Notice:Undefine対策）
        global $arrName_list;

        foreach ($arrName_list as $value) {
            $arrValue[$value] = isset($arrValue[$value]) ? $arrValue[$value] : '';
            $arrErrorText[$value] = isset($arrErrorText[$value]) ? $arrErrorText[$value] : '';
        }

        $arrValue['action'] = isset($arrValue['action']) ? $arrValue['action'] : '';
        $arrValue['return_flg'] = isset($arrValue['return_flg']) ? $arrValue['return_flg'] : '';

        $arrErrorText['token_err'] = isset($arrErrorText['token_err']) ? $arrErrorText['token_err'] : '';

        $checked_agree_check = isset($arrValue['agree_check']) ? 'checked' : '';

        // return_flgの値を設定
        $return_flg = ($arrValue['return_flg'] !== '' or isset($arrErrorText['return_flg'])) ? 'return' : '';


        if (isset($arrValue['proposal_content'])) {
            switch ($arrValue['proposal_content']) {
                case '満足':
                    $checked_proposal_content5 = 'checked';
                    break;
                case 'ほぼ満足':
                    $checked_proposal_content4 = 'checked';
                    break;
                case '普通':
                    $checked_proposal_content3 = 'checked';
                    break;
                case 'やや不満':
                    $checked_proposal_content2 = 'checked';
                    break;
                case '不満':
                    $checked_proposal_content1 = 'checked';
                    break;
            }
        }

        if (isset($arrValue['Personin_charge'])) {
            switch ($arrValue['Personin_charge']) {
                case '満足':
                    $checked_Personin_charge5 = 'checked';
                    break;
                case 'ほぼ満足':
                    $checked_Personin_charge4 = 'checked';
                    break;
                case '普通':
                    $checked_Personin_charge3 = 'checked';
                    break;
                case 'やや不満':
                    $checked_Personin_charge2 = 'checked';
                    break;
                case '不満':
                    $checked_Personin_charge1 = 'checked';
                    break;
            }
        }

        if (isset($arrValue['achievement'])) {
            switch ($arrValue['achievement']) {
                case '満足':
                    $checked_achievement5 = 'checked';
                    break;
                case 'ほぼ満足':
                    $checked_achievement4 = 'checked';
                    break;
                case '普通':
                    $checked_achievement3 = 'checked';
                    break;
                case 'やや不満':
                    $checked_achievement2 = 'checked';
                    break;
                case '不満':
                    $checked_achievement1 = 'checked';
                    break;
            }
        }

        if (isset($arrValue['worker_response'])) {
            switch ($arrValue['worker_response']) {
                case '満足':
                    $checked_worker_response5 = 'checked';
                    break;
                case 'ほぼ満足':
                    $checked_worker_response4 = 'checked';
                    break;
                case '普通':
                    $checked_worker_response3 = 'checked';
                    break;
                case 'やや不満':
                    $checked_worker_response2 = 'checked';
                    break;
                case '不満':
                    $checked_worker_response1 = 'checked';
                    break;
            }
        }

        if (isset($arrValue['due_date'])) {
            switch ($arrValue['due_date']) {
                case '満足':
                    $checked_due_date5 = 'checked';
                    break;
                case 'ほぼ満足':
                    $checked_due_date4 = 'checked';
                    break;
                case '普通':
                    $checked_due_date3 = 'checked';
                    break;
                case 'やや不満':
                    $checked_due_date2 = 'checked';
                    break;
                case '不満':
                    $checked_due_date1 = 'checked';
                    break;
            }
        }

        if (isset($arrValue['evaluation'])) {
            switch ($arrValue['evaluation']) {
                case '満足':
                    $checked_evaluation5 = 'checked';
                    break;
                case 'ほぼ満足':
                    $checked_evaluation4 = 'checked';
                    break;
                case '普通':
                    $checked_evaluation3 = 'checked';
                    break;
                case 'やや不満':
                    $checked_evaluation2 = 'checked';
                    break;
                case '不満':
                    $checked_evaluation1 = 'checked';
                    break;
            }
        }


        //
        $checked_AH1 = isset($_POST['AH1']) ? 'checked' : ''; $checked_AH2 = isset($_POST['AH2']) ? 'checked' : '';
        $checked_AH3 = isset($_POST['AH3']) ? 'checked' : ''; $checked_AH4 = isset($_POST['AH4']) ? 'checked' : '';
        $checked_AH5 = isset($_POST['AH5']) ? 'checked' : ''; $checked_AH6 = isset($_POST['AH6']) ? 'checked' : '';
        $checked_AH7 = isset($_POST['AH7']) ? 'checked' : '';

        $checked_BH1 = isset($_POST['BH1']) ? 'checked' : ''; $checked_BH2 = isset($_POST['BH2']) ? 'checked' : '';
        $checked_BH3 = isset($_POST['BH3']) ? 'checked' : ''; $checked_BH4 = isset($_POST['BH4']) ? 'checked' : '';
        $checked_BH5 = isset($_POST['BH5']) ? 'checked' : ''; $checked_BH6 = isset($_POST['BH6']) ? 'checked' : '';

        $checked_CH1 = isset($_POST['CH1']) ? 'checked' : ''; $checked_CH2 = isset($_POST['CH2']) ? 'checked' : '';
        $checked_CH3 = isset($_POST['CH3']) ? 'checked' : ''; $checked_CH4 = isset($_POST['CH4']) ? 'checked' : '';
        $checked_CH5 = isset($_POST['CH5']) ? 'checked' : ''; $checked_CH6 = isset($_POST['CH6']) ? 'checked' : '';
        $checked_CH7 = isset($_POST['CH7']) ? 'checked' : ''; $checked_CH8 = isset($_POST['CH8']) ? 'checked' : '';
        $checked_CH9 = isset($_POST['CH9']) ? 'checked' : ''; $checked_CH10 = isset($_POST['CH10']) ? 'checked' : '';

        $checked_DH1 = isset($_POST['DH1']) ? 'checked' : ''; $checked_DH2 = isset($_POST['DH2']) ? 'checked' : '';
        $checked_DH3 = isset($_POST['DH3']) ? 'checked' : ''; $checked_DH4 = isset($_POST['DH4']) ? 'checked' : '';
        $checked_DH5 = isset($_POST['DH5']) ? 'checked' : ''; $checked_DH6 = isset($_POST['DH6']) ? 'checked' : '';
        $checked_DH7 = isset($_POST['DH7']) ? 'checked' : ''; $checked_DH8 = isset($_POST['DH8']) ? 'checked' : '';

        $checked_EH1 = isset($_POST['EH1']) ? 'checked' : ''; $checked_EH2 = isset($_POST['EH2']) ? 'checked' : '';
        $checked_EH3 = isset($_POST['EH3']) ? 'checked' : ''; $checked_EH4 = isset($_POST['EH4']) ? 'checked' : '';
        $checked_EH5 = isset($_POST['EH5']) ? 'checked' : ''; $checked_EH6 = isset($_POST['EH6']) ? 'checked' : '';


        echo <<< EOM

        <!-- main -->
        <main>



    <!-- 見積もり依頼・お問合せフォーム -->
    <div class="form_wrap" id="form">
    </div>

                        <div id="main">
                        <div id="cont_area">
                            <div id="cont_box">
                                <a name="form"></a>

        <link rel="stylesheet" href="./css/validationEngine.jquery.css">

        
         </div>

              <span class="critical_error_text">{$arrErrorText["token_err"]}</span>
        <noscript>
            <span class="critical_error_text">JavaScriptを有効にしてください</span>
        </noscript>

        <!-- Newyear Comment-->
        <!--<script src="./js/closure_contact.js"></script>-->

        <div id="form_flow">
                <section class="active_input form_flow_sp">
                    <p>１.<br>必要事項の<br>ご入力</p>
                </section>
                <span class="formflow_arrows form_flow_sp">
                    <i class="fa fa-caret-right fa-1" aria-hidden="true"></i>
                </span>
                <section class="deact_input form_flow_sp">
                    <p>２.<br>ご入力内容の<br>ご確認</p>
                </section>
                <span class="formflow_arrows form_flow_sp">
                    <i class="fa fa-caret-right fa-1" aria-hidden="true"></i>
                </span>
                <section class="deact_input form_flow_sp">
                    <p>３.<br>送信完了</p>&nbsp;
                </section>
            </div>

            <div id="form_flow">
                <section class="active_input form_flow_pc">
                    <p>１.必要事項のご入力</p>
                </section>
                <span class="formflow_arrows form_flow_pc">
                    <i class="fa fa-caret-right fa-1" aria-hidden="true"></i>
                    <i class="fa fa-caret-right fa-1" aria-hidden="true"></i>
                    <i class="fa fa-caret-right fa-1" aria-hidden="true"></i>
                </span>
                <section class="deact_input form_flow_pc">
                    <p>２.ご入力内容のご確認</p>
                </section>
                <span class="formflow_arrows form_flow_pc">
                    <i class="fa fa-caret-right fa-1" aria-hidden="true"></i>
                    <i class="fa fa-caret-right fa-1" aria-hidden="true"></i>
                    <i class="fa fa-caret-right fa-1" aria-hidden="true"></i>
                </span>
                <section class="deact_input form_flow_pc">
                    <p>３.送信完了</p>
                </section>
            </div>

        <form name="form1" id="form1" class="btform1" method="post" enctype="multipart/form-data" action="?">
            <input type="hidden" name="action" id="action" value="">
            <input type="hidden" name="token" value="{$_SESSION['token']}">
            <input type="hidden" name="return_flg" value="{$return_flg}">

            <div class="form_title">ご契約者様情報</div>
            <table>
            <tbody>

                <tr>
                    <th>
                        <span class="label label-danger required_label" id="required_label_name">必須</span>
                        <span class="label label-success ok_label" id="ok_label_name">ＯＫ</span>
                        <label for="name">ご契約者名</label>
                    </th>
                    <td>
                        <span class="error_text">{$arrErrorText["name"]}</span>
                        <input type="text" name="name" id="name"
                        class="validate_input required_input long_text validate[required]"
                        value="{$arrValue["name"]}"
                        placeholder="例：東急太郎"
                        onblur="fncTextValidate('name');fncTextValidate('name_kana')"/>
                    </td>
                </tr>
                <tr>
                    <th>
                        <span class="label label-danger required_label" id="required_label_name_kana">必須</span>
                        <span class="label label-success ok_label" id="ok_label_name_kana">ＯＫ</span>
                        <label for="name_kana">ご契約者名(カナ）</label>
                    </th>
                    <td>
                      <span class="error_text">{$arrErrorText["name_kana"]}</span>
                      <input type="text" name="name_kana" id="name_kana"
                      class="validate_input required_input long_text validate[required]"
                      value="{$arrValue["name_kana"]}"
                      placeholder="例：トウキュウタロウ"
                      onblur="fncTextValidate('name_kana')"/>
                    </td>
                </tr>
                <tr>
                    <th>
                        <span class="label label-danger required_label" id="required_label_name_mansion">必須</span>
                        <span class="label label-success ok_label" id="ok_label_name_mansion">ＯＫ</span>
                        <label for="name_mansion">マンション名</label>
                    </th>
                    <td>
                      <span class="error_text">{$arrErrorText["name_mansion"]}</span>
                      <input type="text" name="name_mansion" id="name_mansion"
                      class="validate_input required_input long_text validate[required]"
                      value="{$arrValue["name_mansion"]}"
                      placeholder="例："
                      onblur="fncTextValidate('name_mansion')"/>
                    </td>
                </tr>
                <tr>
                    <th>
                        <span class="label label-danger required_label" id="required_label_room_number">必須</span>
                        <span class="label label-success ok_label" id="ok_label_room_number">ＯＫ</span>
                        <label for="room_number">お部屋番号</label>
                    </th>
                    <td>
                      <span class="error_text">{$arrErrorText["room_number"]}</span>
                      <input type="text" name="room_number" id="room_number"
                      class="validate_input required_input long_text validate[required]"
                      value="{$arrValue["room_number"]}"
                      placeholder="例："
                      onblur="fncTextValidate('room_number')"/>
                    </td>
                </tr>
                <tr>
                    <th>
                        <span class="label label-danger required_label" id="required_label_Mail">必須</span>
                        <span class="label label-success ok_label" id="ok_label_Mail">ＯＫ</span>
                        <label for="Mail">メールアドレス</label>
                    </th>
                    <td>
                        <span class="error_text">{$arrErrorText["Mail"]}</span>
                        <input type="text" name="Mail" id="Mail"
                        class="validate_input required_input middle_text validate[required,custom[email]]"
                        value="{$arrValue["Mail"]}"
                        placeholder="例：abc@aaa.co.jp"
                        onblur="fncConvertToNarrow('Mail'); fncTextValidate('Mail');"/>
                        <!--<br>
                        <span class="error_text">{$arrErrorText["Mail2"]}</span>
                        <input type="text" name="Mail" id="Mail2"
                        class="validate_input required_input middle_text validate[required, equals[Mail], custom[email]]"
                        value="{$arrValue["Mail2"]}"
                        placeholder="例：abc@aaa.co.jp"
                        onblur="fncConvertToNarrow('Mail2'); fncTextValidate('Mail2');"/>-->
                    </td>
                </tr>
                </tbody>
                </table>
                <br>

                <div class="ttl_questionaire_form">
                    <div class="form_title">アンケート</div>
                </div>

                <div class="level_based_ans">
                    <span class="label label-danger required_label" id="required_label_grp_radio">必須</span>
                    <span class="label label-success ok_label" id="ok_label_grp_radio">ＯＫ</span>
                    <label for="check_trigr">Q1.今回のリフォーム工事に対するご意見をお聞かせください。</label>
                </div>
                <br>
                
                <table>
                <tbody>


                    <tr>
                        <table class="ans_level">
                            <tr>
                            <td>項目</td>
                            <td>満足</td>
                            <td>ほぼ満足</td>
                            <td>普通</td>
                            <td>やや不満</td>
                            <td>不満</td>
                            </tr>
                            <tr class="grp_radio">
                            <td>
                                <span class="require_txt" id="required_label_proposal_content">必須</span>
                                <span class="checked_icon" id="ok_label_proposal_content">
                                    <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                        <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none"/>
                                        <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8"/></svg>
                                </span>
                                <span class="grp_radio_txt">弊社の提案内容</span>
                            </td>
                            <td>
                                <input type="radio"
                                name="proposal_content"
                                id="proposal_content5"
                                {$checked_proposal_content5}
                                value="満足"
                                style="vertical-align:middle;"
                                class="validate[required] grp_radio_all"
                                data-prompt-position="topRight"/>
                                <label for="proposal_content5">5</label><label class="sp" for="proposal_content5">（満足）</label>
                            </td>
                            <td>
                                <input type="radio"
                                name="proposal_content"
                                id="proposal_content4"
                                {$checked_proposal_content4}
                                value="ほぼ満足"
                                style="vertical-align:middle;"
                                class="validate[required] grp_radio_all"
                                data-prompt-position="topRight"/>
                            <label for="proposal_content4">4</label><label class="sp" for="proposal_content4">（ほぼ満足）</label>
                            </td>
                            <td>
                                <input type="radio"
                                name="proposal_content"
                                id="proposal_content3"
                                {$checked_proposal_content3}
                                value="普通"
                                style="vertical-align:middle;"
                                class="validate[required] grp_radio_all"
                                data-prompt-position="topRight"/>
                            <label for="proposal_content3">3</label><label class="sp" for="proposal_content3">（普通）</label>
                            </td>
                            <td>
                                <input type="radio"
                                name="proposal_content"
                                id="proposal_content2"
                                {$checked_proposal_content2}
                                value="やや不満"
                                style="vertical-align:middle;"
                                class="validate[required] grp_radio_all"
                                data-prompt-position="topRight"/>
                            <label for="proposal_content2">2</label><label class="sp" for="proposal_content2">（やや不満）</label>
                            </td>
                            <td>
                                <input type="radio"
                                name="proposal_content"
                                id="proposal_content1"
                                {$checked_proposal_content1}
                                value="不満"
                                style="vertical-align:middle;"
                                class="validate[required] grp_radio_all"
                                data-prompt-position="topRight"/>
                            <label for="proposal_content1">1</label><label class="sp" for="proposal_content1">（不満）</label>
                            </td>
                            </tr>
                            <tr class="grp_radio">
                            <td>
                                <span class="require_txt" id="required_label_Personin_charge">必須</span>
                                <span class="checked_icon" id="ok_label_Personin_charge">
                                    <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                        <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none"/>
                                        <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8"/></svg>
                                </span>
                                <span class="grp_radio_txt">担当者の対応</span>
                            </td>
                            <td>
                                <input type="radio"
                                name="Personin_charge"
                                id="Personin_charge5"
                                {$checked_Personin_charge5}
                                value="満足"
                                style="vertical-align:middle;"
                                class="validate[required] grp_radio_all"
                                data-prompt-position="topRight"/>
                            <label for="Personin_charge5">5</label><label class="sp" for="Personin_charge5">（満足）</label>
                            </td>
                            <td>
                                <input type="radio"
                                name="Personin_charge"
                                id="Personin_charge4"
                                {$checked_Personin_charge4}
                                value="ほぼ満足"
                                style="vertical-align:middle;"
                                class="validate[required] grp_radio_all"
                                data-prompt-position="topRight"/>
                            <label for="Personin_charge4">4</label><label class="sp" for="Personin_charge4">（ほぼ満足）</label>
                            </td>
                            <td>
                                <input type="radio"
                                name="Personin_charge"
                                id="Personin_charge3"
                                {$checked_Personin_charge3}
                                value="普通"
                                style="vertical-align:middle;"
                                class="validate[required] grp_radio_all"
                                data-prompt-position="topRight"/>
                            <label for="Personin_charge3">3</label><label class="sp" for="Personin_charge3">（普通）</label>
                            </td>
                            <td>
                                <input type="radio"
                                name="Personin_charge"
                                id="Personin_charge2"
                                {$checked_Personin_charge2}
                                value="やや不満"
                                style="vertical-align:middle;"
                                class="validate[required] grp_radio_all"
                                data-prompt-position="topRight"/>
                            <label for="Personin_charge2">2</label><label class="sp" for="Personin_charge2">（やや不満）</label>
                            </td>
                            <td>
                                <input type="radio"
                                name="Personin_charge"
                                id="Personin_charge1"
                                {$checked_Personin_charge1}
                                value="不満"
                                style="vertical-align:middle;"
                                class="validate[required] grp_radio_all"
                                data-prompt-position="topRight"/>
                            <label for="Personin_charge1">1</label><label class="sp" for="Personin_charge1">（不満）</label>
                            </td>
                            </tr>
                            <tr class="grp_radio">
                            <td>
                                <span class="require_txt" id="required_label_achievement">必須</span>
                                <span class="checked_icon" id="ok_label_achievement">
                                    <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                        <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none"/>
                                        <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8"/></svg>
                                </span>
                                <span class="grp_radio_txt">工事の出来栄え</span>
                            </td>
                            <td>
                                <input type="radio"
                                name="achievement"
                                id="achievement5"
                                {$checked_achievement5}
                                value="満足"
                                style="vertical-align:middle;"
                                class="validate[required] grp_radio_all"
                                data-prompt-position="topRight"/>
                            <label for="achievement5">5</label><label class="sp" for="achievement5">（満足）</label>
                            </td>
                            <td>
                                <input type="radio"
                                name="achievement"
                                id="achievement4"
                                {$checked_achievement4}
                                value="ほぼ満足"
                                style="vertical-align:middle;"
                                class="validate[required] grp_radio_all"
                                data-prompt-position="topRight"/>
                            <label for="achievement4">4</label><label class="sp" for="achievement4">（ほぼ満足）</label>
                            </td>
                            <td>
                                <input type="radio"
                                name="achievement"
                                id="achievement3"
                                {$checked_achievement3}
                                value="普通"
                                style="vertical-align:middle;"
                                class="validate[required] grp_radio_all"
                                data-prompt-position="topRight"/>
                            <label for="achievement3">3</label><label class="sp" for="achievement3">（普通）</label>
                            </td>
                            <td>
                                <input type="radio"
                                name="achievement"
                                id="achievement2"
                                {$checked_achievement2}
                                value="やや不満"
                                style="vertical-align:middle;"
                                class="validate[required] grp_radio_all"
                                data-prompt-position="topRight"/>
                            <label for="achievement2">2</label><label class="sp" for="achievement2">（やや不満）</label>
                            </td>
                            <td>
                                <input type="radio"
                                name="achievement"
                                id="achievement1"
                                {$checked_achievement1}
                                value="不満"
                                style="vertical-align:middle;"
                                class="validate[required] grp_radio_all"
                                data-prompt-position="topRight"/>
                            <label for="achievement1">1</label><label class="sp" for="achievement1">（不満）</label>
                            </td>
                            </tr>
                            <tr class="grp_radio">
                            <td>
                                <span class="require_txt" id="required_label_worker_response">必須</span>
                                <span class="checked_icon" id="ok_label_worker_response">
                                    <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                        <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none"/>
                                        <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8"/></svg>
                                </span>
                                <span class="grp_radio_txt">現場作業員の対応</span>
                            </td>
                            <td>
                                <input type="radio"
                                name="worker_response"
                                id="worker_response5"
                                {$checked_worker_response5}
                                value="満足"
                                style="vertical-align:middle;"
                                class="validate[required] grp_radio_all"
                                data-prompt-position="topRight"/>
                            <label for="worker_response5">5</label><label class="sp" for="worker_response5">（満足）</label>
                            </td>
                            <td>
                                <input type="radio"
                                name="worker_response"
                                id="worker_response4"
                                {$checked_worker_response4}
                                value="ほぼ満足"
                                style="vertical-align:middle;"
                                class="validate[required] grp_radio_all"
                                data-prompt-position="topRight"/>
                            <label for="worker_response4">4</label><label class="sp" for="worker_response4">（ほぼ満足）</label>
                            </td>
                            <td>
                                <input type="radio"
                                name="worker_response"
                                id="worker_response3"
                                {$checked_worker_response3}
                                value="普通"
                                style="vertical-align:middle;"
                                class="validate[required] grp_radio_all"
                                data-prompt-position="topRight"/>
                            <label for="worker_response3">3</label><label class="sp" for="worker_response3">（普通）</label>
                            </td>
                            <td>
                                <input type="radio"
                                name="worker_response"
                                id="worker_response2"
                                {$checked_worker_response2}
                                value="やや不満"
                                style="vertical-align:middle;"
                                class="validate[required] grp_radio_all"
                                data-prompt-position="topRight"/>
                            <label for="worker_response2">2</label><label class="sp" for="worker_response2">（やや不満）</label>
                            </td>
                            <td>
                                <input type="radio"
                                name="worker_response"
                                id="worker_response1"
                                {$checked_worker_response1}
                                value="不満"
                                style="vertical-align:middle;"
                                class="validate[required] grp_radio_all"
                                data-prompt-position="topRight"/>
                            <label for="worker_response1">1</label><label class="sp" for="worker_response1">（不満）</label>
                            </td>
                            </tr>
                            <tr class="grp_radio">
                            <td>
                                <span class="require_txt" id="required_label_due_date">必須</span>
                                <span class="checked_icon" id="ok_label_due_date">
                                    <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                        <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none"/>
                                        <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8"/></svg>
                                </span>
                                <span class="grp_radio_txt">施工管理／期日</span>
                            </td>
                            <td>
                                <input type="radio"
                                name="due_date"
                                id="due_date5"
                                {$checked_due_date5}
                                value="満足"
                                style="vertical-align:middle;"
                                class="validate[required] grp_radio_all"
                                data-prompt-position="topRight"/>
                            <label for="due_date5">5</label><label class="sp" for="due_date5">（満足）</label>
                            </td>
                            <td>
                                <input type="radio"
                                name="due_date"
                                id="due_date4"
                                {$checked_due_date4}
                                value="ほぼ満足"
                                style="vertical-align:middle;"
                                class="validate[required] grp_radio_all"
                                data-prompt-position="topRight"/>
                            <label for="due_date4">4</label><label class="sp" for="due_date4">（ほぼ満足）</label>
                            </td>
                            <td>
                                <input type="radio"
                                name="due_date"
                                id="due_date3"
                                {$checked_due_date3}
                                value="普通"
                                style="vertical-align:middle;"
                                class="validate[required] grp_radio_all"
                                data-prompt-position="topRight"/>
                            <label for="due_date3">3</label><label class="sp" for="due_date3">（普通）</label>
                            </td>
                            <td>
                                <input type="radio"
                                name="due_date"
                                id="due_date2"
                                {$checked_due_date2}
                                value="やや不満"
                                style="vertical-align:middle;"
                                class="validate[required] grp_radio_all"
                                data-prompt-position="topRight"/>
                            <label for="due_date2">2</label><label class="sp" for="due_date2">（やや不満）</label>
                            </td>
                            <td>
                                <input type="radio"
                                name="due_date"
                                id="due_date1"
                                {$checked_due_date1}
                                value="不満"
                                style="vertical-align:middle;"
                                class="validate[required] grp_radio_all"
                                data-prompt-position="topRight"/>
                            <label for="due_date1">1</label><label class="sp" for="due_date1">（不満）</label>
                            </td>
                            </tr>
                            <tr class="grp_radio">
                            <td>
                                <span class="require_txt" id="required_label_evaluation">必須</span>
                                <span class="checked_icon" id="ok_label_evaluation">
                                    <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                        <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none"/>
                                        <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8"/></svg>
                                </span>
                                <span class="grp_radio_txt">総合評価</span>
                            </td>
                            <td>
                                <input type="radio"
                                name="evaluation"
                                id="evaluation5"
                                {$checked_evaluation5}
                                value="満足"
                                style="vertical-align:middle;"
                                class="validate[required] grp_radio_all"
                                data-prompt-position="topRight"/>
                            <label for="evaluation5">5</label><label class="sp" for="evaluation5">（満足）</label>
                            </td>
                            <td>
                                <input type="radio"
                                name="evaluation"
                                id="evaluation4"
                                {$checked_evaluation4}
                                value="ほぼ満足"
                                style="vertical-align:middle;"
                                class="validate[required] grp_radio_all"
                                data-prompt-position="topRight"/>
                            <label for="evaluation4">4</label><label class="sp" for="evaluation4">（ほぼ満足）</label>
                            </td>
                            <td>
                                <input type="radio"
                                name="evaluation"
                                id="evaluation3"
                                {$checked_evaluation3}
                                value="普通"
                                style="vertical-align:middle;"
                                class="validate[required] grp_radio_all"
                                data-prompt-position="topRight"/>
                            <label for="evaluation3">3</label><label class="sp" for="evaluation3">（普通）</label>
                            </td>
                            <td>
                                <input type="radio"
                                name="evaluation"
                                id="evaluation2"
                                {$checked_evaluation2}
                                value="やや不満"
                                style="vertical-align:middle;"
                                class="validate[required] grp_radio_all"
                                data-prompt-position="topRight"/>
                            <label for="evaluation2">2</label><label class="sp" for="evaluation2">（やや不満）</label>
                            </td>
                            <td>
                                <input type="radio"
                                name="evaluation"
                                id="evaluation1"
                                {$checked_evaluation1}
                                value="不満"
                                style="vertical-align:middle;"
                                class="validate[required] grp_radio_all"
                                data-prompt-position="topRight"/>
                            <label for="evaluation1">1</label><label class="sp" for="evaluation1">（不満）</label>
                            </td>
                            </tr>
                        </table>
                    </tr>

                </tbody>
            </table>


            <br><br>

            <div class="question_list_wrap">
                <div class="question_list_ttl_lbl">
                    <span class="label label-danger required_label" id="required_label_check_trigr_AH">必須</span>
                    <span class="label label-success ok_label" id="ok_label_check_trigr_AH">ＯＫ</span>
                    <label for="check_trigr">Q2.弊社にご発注いただいた決め手は何ですか。（複数回答可）</label>
                </div>
                <div class="question_list question_list_section2">
                    <ul class="list">
                    <li>
                        <input type="checkbox" {$checked_AH1} name="AH1" id="AH1"
                          value="担当者の対応が良かった"
                          class="group_trigger_AH"
                          data-prompt-position="topRight"
                          style="vertical-align:middle;">
                        <label for="AH1"> 担当者の対応が良かった&nbsp;</label>
                    </li>
                    <li>
                        <input name="AH2" id="AH2" type="checkbox" {$checked_AH2}
                          value="提案内容が良かった"
                          class="group_trigger_AH"
                          style="vertical-align:middle;">
                        <label for="AH2"> 提案内容が良かった&nbsp;</label>
                    </li>
                    <li>
                        <input name="AH3" id="AH3" type="checkbox" {$checked_AH3}
                          value="会社が信頼できた"
                          class="group_trigger_AH"
                          style="vertical-align:middle;">
                        <label for="AH3"> 会社が信頼できた&nbsp;</label>
                    </li>
                    <li>
                        <input type="checkbox" {$checked_AH4} name="AH4" id="AH4"
                          value="評判が良かった・紹介された"
                          class="group_trigger_AH"
                          style="vertical-align:middle;">
                        <label for="AH4"> 評判が良かった・紹介された&nbsp;</label>
                    </li>
                    <li>
                        <input name="AH5" id="AH5" type="checkbox" {$checked_AH5}
                          value="アフターサービスが充実している"
                          class="group_trigger_AH"
                          style="vertical-align:middle;">
                        <label for="AH5"> アフターサービスが充実している&nbsp;</label>
                    </li>
                    <li>
                        <input name="AH6" id="AH6" type="checkbox" {$checked_AH6}
                          value="明確な価格表示で安心できた"
                          class="group_trigger_AH"
                          style="vertical-align:middle;">
                        <label for="AH6"> 明確な価格表示で安心できた&nbsp;</label>
                    </li>
                    <li>
                        <input name="AH7" id="AH7" type="checkbox" {$checked_AH7}
                          value="その他："
                          class="group_trigger_AH"
                          style="vertical-align:middle;">
                        <label for="AH7"> その他&nbsp;</label>
                        <input type="text" name="AH7_by_others" id="AH7_by_others"
                        class="" value="{$arrValue["AH7_by_others"]}"
                        placeholder=""　 style="color: #000000;" disabled/>
                    </li>
                    </ul>
                </div>
            </div>
                
            <div class="question_list_wrap">
                <div class="question_list_ttl_lbl">
                    <span class="label label-danger required_label" id="required_label_check_trigr_BH">必須</span>
                    <span class="label label-success ok_label" id="ok_label_check_trigr_BH">ＯＫ</span>
                    <label for="check_trigr">Q3.今後リフォームを検討する際、関心がある内容は何ですか。（複数回答可）</label>
                </div>
                <div class="question_list question_list_section3">
                    <ul class="list">
                    <li>
                        <input type="checkbox" {$checked_BH1} name="BH1" id="BH1"
                          value="全面改装"
                          class="group_trigger_BH"
                          onChange="fncCheckboxGroupValidate_trigr()"
                          data-prompt-position="topRight"
                          style="vertical-align:middle;">
                        <label for="BH1"> 全面改装&nbsp;</label>
                    </li>
                    <li>
                        <input name="BH2" id="BH2" type="checkbox" {$checked_BH2}
                          value="間取り変更"
                          class="group_trigger_BH"
                          onChange="fncCheckboxGroupValidate_trigr()"
                          style="vertical-align:middle;">
                        <label for="BH2"> 間取り変更&nbsp;</label>
                    </li>
                    <li>
                        <input name="BH3" id="BH3" type="checkbox" {$checked_BH3}
                          value="模様替え"
                          class="group_trigger_BH"
                          onChange="fncCheckboxGroupValidate_trigr()"
                          data-prompt-position="topRight"
                          style="vertical-align:middle;">
                        <label for="BH3"> 模様替え&nbsp;</label>
                    </li>
                    <li>
                        <input type="checkbox" {$checked_BH4} name="BH4" id="BH4"
                          value="設備機器交換"
                          class="group_trigger_BH"
                          onChange="fncCheckboxGroupValidate_trigr()"
                          data-prompt-position="topRight"
                          style="vertical-align:middle;">
                        <label for="BH4"> 設備機器交換&nbsp;</label>
                    </li>
                    <li>
                        <input name="BH5" id="BH5" type="checkbox" {$checked_BH5}
                          value="配管更新"
                          class="group_trigger_BH"
                          onChange="fncCheckboxGroupValidate_trigr()"
                          style="vertical-align:middle;">
                        <label for="BH5"> 配管更新&nbsp;</label>
                    </li>
                    <li>
                        <input name="BH6" id="BH6" type="checkbox" {$checked_BH6}
                          value="その他："
                          class="group_trigger_BH"
                          onChange="fncCheckboxGroupValidate_trigr()"
                          data-prompt-position="topRight"
                          style="vertical-align:middle;">
                        <label for="BH6"> その他&nbsp;</label>
                        <input type="text" name="BH6_by_others" id="BH6_by_others"
                        class="" value="{$arrValue["BH6_by_others"]}"
                        placeholder=""　 style="color: #000000;" disabled/>
                    </li>
                    </ul>
                </div>
            </div>
                
            <div class="question_list_wrap">
                <div class="question_list_ttl_lbl">
                    <span class="label label-danger required_label" id="required_label_check_trigr_CH">必須</span>
                    <span class="label label-success ok_label" id="ok_label_check_trigr_CH">ＯＫ</span>
                    <label for="check_trigr">Q4.今後リフォームを検討したい部位（場所）は何処ですか。（複数回答可）</label>
                </div>
                <div class="question_list question_list_section4">
                    <ul class="list">
                    <li>
                        <input type="checkbox" {$checked_CH1} name="CH1" id="CH1"
                          value="リビング"
                          class="group_trigger_CH"
                          onChange="fncCheckboxGroupValidate_trigr()"
                          data-prompt-position="topRight"
                          style="vertical-align:middle;">
                        <label for="CH1"> リビング&nbsp;</label>
                    </li>
                    <li>
                        <input name="CH2" id="CH2" type="checkbox" {$checked_CH2}
                          value="キッチン"
                          class="group_trigger_CH"
                          onChange="fncCheckboxGroupValidate_trigr()"
                          style="vertical-align:middle;">
                        <label for="CH2"> キッチン&nbsp;</label>
                    </li>
                    <li>
                        <input name="CH3" id="CH3" type="checkbox" {$checked_CH3}
                          value="浴室"
                          class="group_trigger_CH"
                          onChange="fncCheckboxGroupValidate_trigr()"
                          data-prompt-position="topRight"
                          style="vertical-align:middle;">
                        <label for="CH3"> 浴室&nbsp;</label>
                    </li>
                    <li>
                        <input type="checkbox" {$checked_CH4} name="CH4" id="CH4"
                          value="洗面室"
                          class="group_trigger_CH"
                          onChange="fncCheckboxGroupValidate_trigr()"
                          data-prompt-position="topRight"
                          style="vertical-align:middle;">
                        <label for="CH4"> 洗面室&nbsp;</label>
                    </li>
                    <li>
                        <input name="CH5" id="CH5" type="checkbox" {$checked_CH5}
                          value="トイレ"
                          class="group_trigger_CH"
                          onChange="fncCheckboxGroupValidate_trigr()"
                          style="vertical-align:middle;">
                        <label for="CH5"> トイレ&nbsp;</label>
                    </li>
                    <li>
                        <input name="CH6" id="CH6" type="checkbox" {$checked_CH6}
                          value="寝室"
                          class="group_trigger_CH"
                          onChange="fncCheckboxGroupValidate_trigr()"
                          data-prompt-position="topRight"
                          style="vertical-align:middle;">
                        <label for="CH6"> 寝室&nbsp;</label>
                    </li>
                    <li>
                        <input type="checkbox" {$checked_CH7} name="CH7" id="CH7"
                          value="子供部屋"
                          class="group_trigger_CH"
                          onChange="fncCheckboxGroupValidate_trigr()"
                          data-prompt-position="topRight"
                          style="vertical-align:middle;">
                        <label for="CH7"> 子供部屋&nbsp;</label>
                    </li>
                    <li>
                        <input name="CH8" id="CH8" type="checkbox" {$checked_CH8}
                          value="玄関"
                          class="group_trigger_CH"
                          onChange="fncCheckboxGroupValidate_trigr()"
                          style="vertical-align:middle;">
                        <label for="CH8"> 玄関&nbsp;</label>
                    </li>
                    <li>
                        <input name="CH9" id="CH9" type="checkbox" {$checked_CH9}
                          value="収納"
                          class="group_trigger_CH"
                          onChange="fncCheckboxGroupValidate_trigr()"
                          data-prompt-position="topRight"
                          style="vertical-align:middle;">
                        <label for="CH9"> 収納&nbsp;</label>
                    </li>
                    <li>
                        <input name="CH10" id="CH10" type="checkbox" {$checked_CH10}
                          value="その他："
                          class="group_trigger_CH"
                          onChange="fncCheckboxGroupValidate_trigr()"
                          data-prompt-position="topRight"
                          style="vertical-align:middle;">
                        <label for="CH10"> その他&nbsp;</label>
                        <input type="text" name="CH10_by_others" id="CH10_by_others"
                        class="" value="{$arrValue["CH10_by_others"]}"
                        placeholder=""　 style="color: #000000;" disabled/>
                    </li>
                    </ul>
                </div>
            </div>
                
            <div class="question_list_wrap">
                <div class="question_list_ttl_lbl">
                    <span class="label label-danger required_label" id="required_label_check_trigr_DH">必須</span>
                    <span class="label label-success ok_label" id="ok_label_check_trigr_DH">ＯＫ</span>
                    <label for="check_trigr">Q5.次回のリフォームのご予定はありますか。またその時期は何時頃ですか。</label>
                </div>
                <div class="question_list question_list_section5">
                    <ul class="list">
                    <li>
                        <input type="checkbox" {$checked_DH1} name="DH1" id="DH1"
                          value="すぐに"
                          class="group_trigger_DH"
                          onChange="fncCheckboxGroupValidate_trigr()"
                          data-prompt-position="topRight"
                          style="vertical-align:middle;">
                        <label for="DH1"> すぐに&nbsp;</label>
                    </li>
                    <li>
                        <input name="DH2" id="DH2" type="checkbox" {$checked_DH2}
                          value="3ヶ月以内"
                          class="group_trigger_DH"
                          onChange="fncCheckboxGroupValidate_trigr()"
                          style="vertical-align:middle;">
                        <label for="DH2"> 3ヶ月以内&nbsp;</label>
                    </li>
                    <li>
                        <input name="DH3" id="DH3" type="checkbox" {$checked_DH3}
                          value="半年以内"
                          class="group_trigger_DH"
                          onChange="fncCheckboxGroupValidate_trigr()"
                          data-prompt-position="topRight"
                          style="vertical-align:middle;">
                        <label for="DH3"> 半年以内&nbsp;</label>
                    </li>
                    <li>
                        <input type="checkbox" {$checked_DH4} name="DH4" id="DH4"
                          value="1年以内"
                          class="group_trigger_DH"
                          onChange="fncCheckboxGroupValidate_trigr()"
                          data-prompt-position="topRight"
                          style="vertical-align:middle;">
                        <label for="DH4"> 1年以内&nbsp;</label>
                    </li>
                    <li>
                        <input type="checkbox" {$checked_DH5} name="DH5" id="DH5"
                          value="3年以内"
                          class="group_trigger_DH"
                          onChange="fncCheckboxGroupValidate_trigr()"
                          data-prompt-position="topRight"
                          style="vertical-align:middle;">
                        <label for="DH5"> 3年以内&nbsp;</label>
                    </li>
                    <li>
                        <input name="DH6" id="DH6" type="checkbox" {$checked_DH6}
                          value="将来的に"
                          class="group_trigger_DH"
                          onChange="fncCheckboxGroupValidate_trigr()"
                          style="vertical-align:middle;">
                        <label for="DH6"> 将来的に&nbsp;</label>
                    </li>
                    <li>
                        <input name="DH7" id="DH7" type="checkbox" {$checked_DH7}
                          value="特に無し"
                          class="group_trigger_DH"
                          onChange="fncCheckboxGroupValidate_trigr()"
                          data-prompt-position="topRight"
                          style="vertical-align:middle;">
                        <label for="DH7"> 特に無し&nbsp;</label>
                    </li>
                    <li>
                        <input type="checkbox" {$checked_DH8} name="DH8" id="DH8"
                          value="その他："
                          class="group_trigger_DH"
                          onChange="fncCheckboxGroupValidate_trigr()"
                          data-prompt-position="topRight"
                          style="vertical-align:middle;">
                        <label for="DH8"> その他&nbsp;</label>
                        <input type="text" name="DH8_by_others" id="DH8_by_others"
                        class="" value="{$arrValue["DH8_by_others"]}"
                        placeholder=""　 style="color: #000000;" disabled/>
                    </li>
                    </ul>
                </div>
            </div>
                
            <div class="question_list_wrap">
                <div class="question_list_ttl_lbl">
                    <span class="label label-danger required_label" id="required_label_check_trigr_EH">必須</span>
                    <span class="label label-success ok_label" id="ok_label_check_trigr_EH">ＯＫ</span>
                    <label for="check_trigr">Q6.今後どのような情報の提供があればよいとお考えですか。（複数回答可）</label>
                </div>
                <div class="question_list question_list_section6">
                    <ul class="list">
                    <li>
                        <input type="checkbox" {$checked_EH1} name="EH1" id="EH1"
                          value="リフォーム事例・商品紹介"
                          class="group_trigger_EH"
                          onChange="fncCheckboxGroupValidate_trigr()"
                          data-prompt-position="topRight"
                          style="vertical-align:middle;">
                        <label for="EH1"> リフォーム事例・商品紹介&nbsp;</label>
                    </li>
                    <li>
                        <input name="EH2" id="EH2" type="checkbox" {$checked_EH2}
                          value="イベント・セミナー等の案内情報"
                          class="group_trigger_EH"
                          onChange="fncCheckboxGroupValidate_trigr()"
                          style="vertical-align:middle;">
                        <label for="EH2"> イベント・セミナー等の案内情報&nbsp;</label>
                    </li>
                    <li>
                        <input name="EH3" id="EH3" type="checkbox" {$checked_EH3}
                          value="マンションライフスタイル提案"
                          class="group_trigger_EH"
                          onChange="fncCheckboxGroupValidate_trigr()"
                          data-prompt-position="topRight"
                          style="vertical-align:middle;">
                        <label for="EH3"> マンションライフスタイル提案&nbsp;</label>
                    </li>
                    <li>
                        <input type="checkbox" {$checked_EH4} name="EH4" id="EH4"
                          value="家具・カーテン・インテリア情報"
                          class="group_trigger_EH"
                          onChange="fncCheckboxGroupValidate_trigr()"
                          data-prompt-position="topRight"
                          style="vertical-align:middle;">
                        <label for="EH4"> 家具・カーテン・インテリア情報&nbsp;</label>
                    </li>
                    <li>
                        <input name="EH5" id="EH5" type="checkbox" {$checked_EH5}
                          value="メンテナンス（修理・修繕・点検）情報"
                          class="group_trigger_EH"
                          onChange="fncCheckboxGroupValidate_trigr()"
                          style="vertical-align:middle;">
                        <label for="EH5"> メンテナンス（修理・修繕・点検）情報&nbsp;</label>
                    </li>
                    <li>
                        <input name="EH6" id="EH6" type="checkbox" {$checked_EH6}
                          value="その他："
                          class="group_trigger_EH"
                          onChange="fncCheckboxGroupValidate_trigr()"
                          data-prompt-position="topRight"
                          style="vertical-align:middle;">
                        <label for="EH6"> その他&nbsp;</label>
                        <input type="text" name="EH6_by_others" id="EH6_by_others"
                        class="" value="{$arrValue["EH6_by_others"]}"
                        placeholder=""　 style="color: #000000;" disabled/>
                    </li>
                    </ul>
                </div>
            </div>
                
                <div>
                    <div>
                        <!--<span class="label label-danger required_label" id="required_label_Note">必須</span>
                        <span class="label label-success ok_label" id="ok_label_Note">ＯＫ</span>-->
                        <label for="Note">その他お気づきの点、ご要望・ご相談がありましたらご記入ください。</label>
                    </div>
                    <div>
                        <textarea rows="8" cols="60" name="Note" id="Note"
                        class="validate_input"
                        value="{$arrValue["Note"]}"
                        onblur="fncTextValidate('Note')"/>{$arrValue["Note"]}</textarea>
                    </div>
                </div>


                <div class="form_title">個人情報の取り扱いについて</div>
                <table>
                <tbody>
                <tr>
                    <th>
                        <span class="label label-danger required_label" id="required_label_agree_check">必須</span>
                        <span class="label label-success ok_label" id="ok_label_agree_check">ＯＫ</span>
                        <br>
                        <label>「個人情報の取り扱いについて」同意</label>
                    </th>
                    <td>
                        お客様の個人情報は、当社の <a class="prvcy_clr" target="_blank" href="https://www.tokyu-re-design.co.jp/privacy/" target="_blank">個人情報保護方針</a>に基づきまして、お取り扱いさせていただきます。<br />ご同意いただけます場合には下のボタンをクリックして、次の項目にお進みください。<br />

                        <input name="agree_check" id="agree_check" type="checkbox" value="「個人情報の取り扱いについて」に同意する。"
                         class="validate[required]"
                         data-prompt-position="topRight"
                         onChange="fncCheckboxGroupValidate(['agree_check'],'agree_check');"
                         {$checked_agree_check}>
                        <label for="agree_check">「個人情報の取り扱いについて」に同意する。</label>
                    </td>
                </tr>
                </tbody>
                </table>
                <br>


                <div id="sendbutton_block">
                <p>入力内容をご確認いただき、宜しければ｢入力内容確認画面へ｣ボタンを押してください。</p>
                <noscript>
                    <span class="critical_error_text">JavaScriptを有効にしてください</span>
                </noscript>
                <p class="button subBlock__button subBlock__button--blue">
                    <input type="button" name="check" id="check" class="btn btn-primary" onclick="fncValidateAll() && fncFormSubmit('check')" value="入力内容確認画面へ">
                </p>
            </div>
        </form>

        <table border="0" cellspacing="0" cellpadding="0" class="btm_agree_sec">
          <tr  aligin="center">
            <td>
                <img src="images/comodo_secure-100x85.gif" alt="Comodo SSL Certificate Secure Site" style="border: 0px;"></td>
            <td style="vertical-align:bottom;">
                <p style="margin-bottom: 0px; margin-left: 15px;">東急Ｒｅ・デザインのウェブサイトはSSL 暗号化通信に対応しています。<br>お客さまの個人情報は暗号化して送信され、第三者に見られることはありません。</p>
            </td>
          </tr>
        </table>
        <br>

        <script src="./js/jquery-1.11.3.min.js"></script>
        <script src="./js/jquery.validationEngine.js"></script>
        <script src="./js/jquery.validationEngine-ja.js"></script>
        <script>
            $(document).ready(function() {
                $('form#form1').validationEngine({
                  promptPosition: "topRight:-100"
                })
                fncControlTrigger();
                //fncValidateControl('location_area_radio2','location_area');
                if (document.forms["form1"].elements["return_flg"].value == 'return') {
                    fncValidateAll();
                }
            });

            $(function() {
                var elementClicked = false;
                $(window).on("beforeunload", function() {
                    if ($('#action').val() != ''){
                        elementClicked = true;
                    }
                    if (!elementClicked) {
                        return "ページ移動を確認します";
                    }
                });
            });

EOM;
            // アンケート項目関連のjsを表示
            displayHtmlQuestionnaireJs();

            echo <<< EOM

            
            $(".group_trigger_AH").change(function(){
                        is_valid_group_trigger_AH();
                    })

                    function is_valid_group_trigger_AH() {
                        group_trigger_AH_flg = false;
                        $("[class*='group_trigger_AH']:not([type='checkbox'])").each(function() {
                            if ($(this).val() !== '') {
                                group_trigger_AH_flg = true;
                                return false;
                            }
                        });
                        $("[class*='group_trigger_AH'][type='checkbox']").each(function() {
                            if ($(this).prop('checked')) {
                                group_trigger_AH_flg = true;
                                return false;
                            }
                        });
                        if (group_trigger_AH_flg == true) {
                            $('#required_label_check_trigr_AH').hide();
                            $('#ok_label_check_trigr_AH').show();
                            $("#AH1").removeClass("validate[required]");
                            $(".AH1formError").remove();
                        } else {
                            $('#required_label_check_trigr_AH').show();
                            $('#ok_label_check_trigr_AH').hide();
                            $("#AH1").addClass("validate[required]");
                            $("#AH1").validationEngine('validate');
                        }


                        return group_trigger_AH_flg;
                    }
            
                    $("input[name=AH7]").change(function(){
                        value = $('input[name=AH7]:checked').val();
                        if (value == "その他：") {
                            $("#AH7_by_others").prop('disabled', false);
                        }else{
                            $("#AH7_by_others").prop('disabled', true);
                        }
                    });

            $(".group_trigger_BH").change(function(){
                        is_valid_group_trigger_BH();
                    })

                    function is_valid_group_trigger_BH() {
                        group_trigger_BH_flg = false;
                        $("[class*='group_trigger_BH']:not([type='checkbox'])").each(function() {
                            if ($(this).val() !== '') {
                                group_trigger_BH_flg = true;
                                return false;
                            }
                        });
                        $("[class*='group_trigger_BH'][type='checkbox']").each(function() {
                            if ($(this).prop('checked')) {
                                group_trigger_BH_flg = true;
                                return false;
                            }
                        });
                        if (group_trigger_BH_flg == true) {
                            $('#required_label_check_trigr_BH').hide();
                            $('#ok_label_check_trigr_BH').show();
                            $("#BH1").removeClass("validate[required]");
                            $(".BH1formError").remove();
                        } else {
                            $('#required_label_check_trigr_BH').show();
                            $('#ok_label_check_trigr_BH').hide();
                            $("#BH1").addClass("validate[required]");
                            $("#BH1").validationEngine('validate');
                        }


                        return group_trigger_BH_flg;
                    }

                    $("input[name=BH6]").change(function(){
                        value = $('input[name=BH6]:checked').val();
                        if (value == "その他：") {
                            $("#BH6_by_others").prop('disabled', false);
                        }else{
                            $("#BH6_by_others").prop('disabled', true);
                        }
                    });

            $(".group_trigger_CH").change(function(){
                        is_valid_group_trigger_CH();
                    })

                    function is_valid_group_trigger_CH() {
                        group_trigger_CH_flg = false;
                        $("[class*='group_trigger_CH']:not([type='checkbox'])").each(function() {
                            if ($(this).val() !== '') {
                                group_trigger_CH_flg = true;
                                return false;
                            }
                        });
                        $("[class*='group_trigger_CH'][type='checkbox']").each(function() {
                            if ($(this).prop('checked')) {
                                group_trigger_CH_flg = true;
                                return false;
                            }
                        });
                        if (group_trigger_CH_flg == true) {
                            $('#required_label_check_trigr_CH').hide();
                            $('#ok_label_check_trigr_CH').show();
                            $("#CH1").removeClass("validate[required]");
                            $(".CH1formError").remove();
                        } else {
                            $('#required_label_check_trigr_CH').show();
                            $('#ok_label_check_trigr_CH').hide();
                            $("#CH1").addClass("validate[required]");
                            $("#CH1").validationEngine('validate');
                        }


                        return group_trigger_CH_flg;
                    }

                    $("input[name=CH10]").change(function(){
                        value = $('input[name=CH10]:checked').val();
                        if (value == "その他：") {
                            $("#CH10_by_others").prop('disabled', false);
                        }else{
                            $("#CH10_by_others").prop('disabled', true);
                        }
                    });
            
            $(".group_trigger_DH").change(function(){
                        is_valid_group_trigger_DH();
                    })

                    function is_valid_group_trigger_DH() {
                        group_trigger_DH_flg = false;
                        $("[class*='group_trigger_DH']:not([type='checkbox'])").each(function() {
                            if ($(this).val() !== '') {
                                group_trigger_DH_flg = true;
                                return false;
                            }
                        });
                        $("[class*='group_trigger_DH'][type='checkbox']").each(function() {
                            if ($(this).prop('checked')) {
                                group_trigger_DH_flg = true;
                                return false;
                            }
                        });
                        if (group_trigger_DH_flg == true) {
                            $('#required_label_check_trigr_DH').hide();
                            $('#ok_label_check_trigr_DH').show();
                            $("#DH1").removeClass("validate[required]");
                            $(".DH1formError").remove();
                        } else {
                            $('#required_label_check_trigr_DH').show();
                            $('#ok_label_check_trigr_DH').hide();
                            $("#DH1").addClass("validate[required]");
                            $("#DH1").validationEngine('validate');
                        }


                        return group_trigger_DH_flg;
                    }

                    $("input[name=DH8]").change(function(){
                        value = $('input[name=DH8]:checked').val();
                        if (value == "その他：") {
                            $("#DH8_by_others").prop('disabled', false);
                        }else{
                            $("#DH8_by_others").prop('disabled', true);
                        }
                    });

            $(".group_trigger_EH").change(function(){
                        is_valid_group_trigger_EH();
                    })

                    function is_valid_group_trigger_EH() {
                        group_trigger_EH_flg = false;
                        $("[class*='group_trigger_EH']:not([type='checkbox'])").each(function() {
                            if ($(this).val() !== '') {
                                group_trigger_EH_flg = true;
                                return false;
                            }
                        });
                        $("[class*='group_trigger_EH'][type='checkbox']").each(function() {
                            if ($(this).prop('checked')) {
                                group_trigger_EH_flg = true;
                                return false;
                            }
                        });
                        if (group_trigger_EH_flg == true) {
                            $('#required_label_check_trigr_EH').hide();
                            $('#ok_label_check_trigr_EH').show();
                            $("#EH1").removeClass("validate[required]");
                            $(".EH1formError").remove();
                        } else {
                            $('#required_label_check_trigr_EH').show();
                            $('#ok_label_check_trigr_EH').hide();
                            $("#EH1").addClass("validate[required]");
                            $("#EH1").validationEngine('validate');
                        }


                        return group_trigger_EH_flg;
                    }

                    $("input[name=EH6]").change(function(){
                        value = $('input[name=EH6]:checked').val();
                        if (value == "その他：") {
                            $("#EH6_by_others").prop('disabled', false);
                        }else{
                            $("#EH6_by_others").prop('disabled', true);
                        }
                    });
            

            

            $('input[name="proposal_content"]:radio').change(function() {
                fncControlproposal_content();
            });

            function fncControlproposal_content() {
                if ($("input[name=proposal_content]").validationEngine('validate')) {
                    $("#required_label_proposal_content").hide();
                    $("#ok_label_proposal_content").css('display', 'inline-block');
                    return true;
                }else{
                    $("#required_label_proposal_content").css('display', 'inline-block');
                    $("#ok_label_proposal_content").hide();
                    return false;
                }
            }

            $('input[name="Personin_charge"]:radio').change(function() {
                fncControlPersonin_charge();
            });

            function fncControlPersonin_charge() {
                if ($("input[name=Personin_charge]").validationEngine('validate')) {
                    $("#required_label_Personin_charge").hide();
                    $("#ok_label_Personin_charge").css('display', 'inline-block');
                    return true;
                }else{
                    $("#required_label_Personin_charge").css('display', 'inline-block');
                    $("#ok_label_Personin_charge").hide();
                    return false;
                }
            }

            $('input[name="achievement"]:radio').change(function() {
                fncControlachievement();
            });

            function fncControlachievement() {
                if ($("input[name=achievement]").validationEngine('validate')) {
                    $("#required_label_achievement").hide();
                    $("#ok_label_achievement").css('display', 'inline-block');
                    return true;
                }else{
                    $("#required_label_achievement").css('display', 'inline-block');
                    $("#ok_label_achievement").hide();
                    return false;
                }
            }

            $('input[name="worker_response"]:radio').change(function() {
                fncControlworker_response();
            });

            function fncControlworker_response() {
                if ($("input[name=worker_response]").validationEngine('validate')) {
                    $("#required_label_worker_response").hide();
                    $("#ok_label_worker_response").css('display', 'inline-block');
                    return true;
                }else{
                    $("#required_label_worker_response").css('display', 'inline-block');
                    $("#ok_label_worker_response").hide();
                    return false;
                }
            }

            $('input[name="due_date"]:radio').change(function() {
                fncControldue_date();
            });

            function fncControldue_date() {
                if ($("input[name=due_date]").validationEngine('validate')) {
                    $("#required_label_due_date").hide();
                    $("#ok_label_due_date").css('display', 'inline-block');
                    return true;
                }else{
                    $("#required_label_due_date").css('display', 'inline-block');
                    $("#ok_label_due_date").hide();
                    return false;
                }
            }

            $('input[name="evaluation"]:radio').change(function() {
                fncControlevaluation();
            });

            function fncControlevaluation() {
                if ($("input[name=evaluation]").validationEngine('validate')) {
                    $("#required_label_evaluation").hide();
                    $("#ok_label_evaluation").css('display', 'inline-block');
                    return true;
                }else{
                    $("#required_label_evaluation").css('display', 'inline-block');
                    $("#ok_label_evaluation").hide();
                    return false;
                }
            }

            $('.grp_radio_all').click(function(){
                    if ($('.grp_radio:not(:has(:radio:checked))').length) {
                        //alert("At least one group is blank");
                        $('#required_label_grp_radio').show();
                        $('#ok_label_grp_radio').hide();
                    }
                    else {
                        //alert("all checked");
                        $('#required_label_grp_radio').hide();
                        $('#ok_label_grp_radio').show();
                    }
                    });

            function fncValidateAll() {
                var pass_flg = true;

                pass_flg = fncControlproposal_content() ? pass_flg : false;
                pass_flg = fncControlPersonin_charge() ? pass_flg : false;
                pass_flg = fncControlachievement() ? pass_flg : false;
                pass_flg = fncControlworker_response() ? pass_flg : false;
                pass_flg = fncControldue_date() ? pass_flg : false;
                pass_flg = fncControlevaluation() ? pass_flg : false;

                if ($('.grp_radio:not(:has(:radio:checked))').length) {
                        $('#required_label_grp_radio').show();
                        $('#ok_label_grp_radio').hide();
                    }
                    else {
                        $('#required_label_grp_radio').hide();
                        $('#ok_label_grp_radio').show();
                    }

                pass_flg = is_valid_group_trigger_AH() ? pass_flg : false;
                pass_flg = is_valid_group_trigger_BH() ? pass_flg : false;
                pass_flg = is_valid_group_trigger_CH() ? pass_flg : false;
                pass_flg = is_valid_group_trigger_DH() ? pass_flg : false;
                pass_flg = is_valid_group_trigger_EH() ? pass_flg : false;

                value = $('input[name=AH7]:checked').val();
                        if (value == "その他：") {
                            $("#AH7_by_others").prop('disabled', false);
                        }else{
                            $("#AH7_by_others").prop('disabled', true);
                        }

                value = $('input[name=BH6]:checked').val();
                        if (value == "その他：") {
                            $("#BH6_by_others").prop('disabled', false);
                        }else{
                            $("#BH6_by_others").prop('disabled', true);
                        }

                value = $('input[name=CH10]:checked').val();
                        if (value == "その他：") {
                            $("#CH10_by_others").prop('disabled', false);
                        }else{
                            $("#CH10_by_others").prop('disabled', true);
                        }

                value = $('input[name=DH8]:checked').val();
                        if (value == "その他：") {
                            $("#DH8_by_others").prop('disabled', false);
                        }else{
                            $("#DH8_by_others").prop('disabled', true);
                        }

                value = $('input[name=EH6]:checked').val();
                        if (value == "その他：") {
                            $("#EH6_by_others").prop('disabled', false);
                        }else{
                            $("#EH6_by_others").prop('disabled', true);
                        }

                pass_flg = $("input[name=Trigger_radio]").validationEngine('validate') ? pass_flg : false;

                pass_flg = fncCheckboxGroupValidate(['agree_check'],'agree_check')? pass_flg : false;

                $(".validate_input").each(function() {
                    pass_flg = fncTextValidate($(this).attr('id')) ? pass_flg : false;
                });

                if($('.formError')[0]){
                    $("html,body").animate({scrollTop:$('.formError').offset().top});
                }               


                return pass_flg;
            }



            function fncValidateControl(check_id,control_area) {
                fncDisplayControl(check_id,control_area);

                if (document.getElementById(check_id).checked == true) {
                    $('.location_area').find('input').each(function() {
                        $(this).addClass('validate_input');
                    });
                } else {
                    $('.location_area').find('input').each(function() {
                        $(this).removeClass('validate_input');
                    });
                }
                return
            }
        </script>

        <script src="./js/jquery.autoKana.js"></script>
        <script type="text/javascript">
            $(document).ready(
                function() {
                    $.fn.autoKana('#name', '#name_kana', {
                        katakana : true
                    });
                });

        </script>

        <script src="//jpostal-1006.appspot.com/jquery.jpostal.js"></script>
        <script>
            $('#zip').jpostal({
                postcode : ['#Zip'],
                address : {'#Address':'%3%4%5'}
            });
            $('#zip').jpostal({
                postcode : ['#Zip2'],
                address : {'#Address2':'%3%4%5'}
            });
        </script>


        <div class="tag">
        <!-- Google Tag Manager -->
        <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WBCWL4"
        height="0" width="0"
        style="display:none;visibility:hidden"></iframe></noscript>
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-WBCWL4');</script>
        <!-- End Google Tag Manager -->

        <!-- Yahoo Code for your Target List -->
        <script type="text/javascript" language="javascript">
        /* <![CDATA[ */
        var yahoo_retargeting_id = 'DH6LU3GI2U';
        var yahoo_retargeting_label = '';
        var yahoo_retargeting_page_type = '';
        var yahoo_retargeting_items = [{item_id: '', category_id: '', price: '',
        quantity: ''}];
        /* ]]> */
        </script>
        <script type="text/javascript" language="javascript" src="https://b92.yahoo.
        co.jp/js/s_retargeting.js"></script>

        <!-- リマーケティング タグの Google コード -->
        <!--------------------------------------------------
        リマーケティング タグは、個人を特定できる情報と関連付けることも、デリケート
        なカテゴリに属するページに設置することも許可されません。タグの設定方法につい
        ては、こちらのページをご覧ください。
        http://google.com/ads/remarketingsetup
        --------------------------------------------------->
        <script type="text/javascript">
        /* <![CDATA[ */
        var google_conversion_id = 833749516;
        var google_custom_params = window.google_tag_params;
        var google_remarketing_only = true;
        /* ]]> */
        </script>
        <script type="text/javascript"
        src="//www.googleadservices.com/pagead/conversion.js">
        </script>
        <noscript>
        <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt=""
        src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/833749516/?g
        uid=ON&amp;script=0"/>
        </div>
        </noscript>

        </div>

EOM;

        // GoogleAnalyticsタグを表示
        displayHtmlGoogleAnalyticsTag('/form/contact/input.php');
    }



    // HTML確認画面を表示
    function displayHtmlConfirmation($arrValue) {

        global $arrName_list;

        foreach ($arrName_list as $value) {
            $arrValue[$value] = isset($arrValue[$value]) ? $arrValue[$value] : '';
        }

        $note_value = str_replace("\n", '<br>', $arrValue['Note']);

        $arrValue['AH_surv'] = '';

        for ($i=1; $i < 7; $i++) {
            if (isset($arrValue["AH$i"])){
                $arrValue['AH_surv'] .= "" .$arrValue["AH$i"] ."<br>\n";
            }
        }

        $arrValue['BH_surv'] = '';

        for ($i=1; $i < 6; $i++) {
            if (isset($arrValue["BH$i"])){
                $arrValue['BH_surv'] .= "" .$arrValue["BH$i"] ."<br>\n";
            }
        }

        $arrValue['CH_surv'] = '';

        for ($i=1; $i < 10; $i++) {
            if (isset($arrValue["CH$i"])){
                $arrValue['CH_surv'] .= "" .$arrValue["CH$i"] ."<br>\n";
            }
        }

        $arrValue['DH_surv'] = '';

        for ($i=1; $i < 8; $i++) {
            if (isset($arrValue["DH$i"])){
                $arrValue['DH_surv'] .= "" .$arrValue["DH$i"] ."<br>\n";
            }
        }

        $arrValue['EH_surv'] = '';

        for ($i=1; $i < 6; $i++) {
            if (isset($arrValue["EH$i"])){
                $arrValue['EH_surv'] .= "" .$arrValue["EH$i"] ."<br>\n";
            }
        }

        $proposal_number ='';
        if (isset($arrValue['proposal_content'])) {
            switch ($arrValue['proposal_content']) {
                case '満足':
                    $proposal_number = '5';
                    break;
                case 'ほぼ満足':
                    $proposal_number = '4';
                    break;
                case '普通':
                    $proposal_number = '3';
                    break;
                case 'やや不満':
                    $proposal_number = '2';
                    break;
                case '不満':
                    $proposal_number = '1';
                    break;
            }
        }

        $Personin_number ='';
        if (isset($arrValue['Personin_charge'])) {
            switch ($arrValue['Personin_charge']) {
                case '満足':
                    $Personin_number = '5';
                    break;
                case 'ほぼ満足':
                    $Personin_number = '4';
                    break;
                case '普通':
                    $Personin_number = '3';
                    break;
                case 'やや不満':
                    $Personin_number = '2';
                    break;
                case '不満':
                    $Personin_number = '1';
                    break;
            }
        }

        $achievement_number ='';
        if (isset($arrValue['achievement'])) {
            switch ($arrValue['achievement']) {
                case '満足':
                    $achievement_number = '5';
                    break;
                case 'ほぼ満足':
                    $achievement_number = '4';
                    break;
                case '普通':
                    $achievement_number = '3';
                    break;
                case 'やや不満':
                    $achievement_number = '2';
                    break;
                case '不満':
                    $achievement_number = '1';
                    break;
            }
        }

        $worker_number ='';
        if (isset($arrValue['worker_response'])) {
            switch ($arrValue['worker_response']) {
                case '満足':
                    $worker_number = '5';
                    break;
                case 'ほぼ満足':
                    $worker_number = '4';
                    break;
                case '普通':
                    $worker_number = '3';
                    break;
                case 'やや不満':
                    $worker_number = '2';
                    break;
                case '不満':
                    $worker_number = '1';
                    break;
            }
        }

        $due_date_number ='';
        if (isset($arrValue['due_date'])) {
            switch ($arrValue['due_date']) {
                case '満足':
                    $due_date_number = '5';
                    break;
                case 'ほぼ満足':
                    $due_date_number = '4';
                    break;
                case '普通':
                    $due_date_number = '3';
                    break;
                case 'やや不満':
                    $due_date_number = '2';
                    break;
                case '不満':
                    $due_date_number = '1';
                    break;
            }
        }

        $evaluation_number ='';
        if (isset($arrValue['evaluation'])) {
            switch ($arrValue['evaluation']) {
                case '満足':
                    $evaluation_number = '5';
                    break;
                case 'ほぼ満足':
                    $evaluation_number = '4';
                    break;
                case '普通':
                    $evaluation_number = '3';
                    break;
                case 'やや不満':
                    $evaluation_number = '2';
                    break;
                case '不満':
                    $evaluation_number = '1';
                    break;
            }
        }



        echo <<< EOM

                        <div id="main">
                        <div id="cont_area">
                            <div id="cont_box">
                                <a name="form"></a>

        <span class="critical_error_text">{$arrErrorText["token_err"]}</span>
        <noscript>
            <span class="critical_error_text">JavaScriptを有効にしてください</span>
        </noscript>

        <div id="form_flow">
                <section class="deact_input form_flow_sp">
                    <p>１.<br>必要事項の<br>ご入力</p>
                </section>
                <span class="formflow_arrows form_flow_sp">
                    <i class="fa fa-caret-right fa-1" aria-hidden="true"></i>
                </span>
                <section class="active_input form_flow_sp">
                    <p>２.<br>ご入力内容の<br>ご確認</p>
                </section>
                <span class="formflow_arrows form_flow_sp">
                    <i class="fa fa-caret-right fa-1" aria-hidden="true"></i>
                </span>
                <section class="deact_input form_flow_sp">
                    <p>３.<br>送信完了</p>&nbsp;
                </section>
            </div>

            <div id="form_flow">
                <section class="deact_input form_flow_pc">
                    <p>１.必要事項のご入力</p>
                </section>
                <span class="formflow_arrows form_flow_pc">
                    <i class="fa fa-caret-right fa-1" aria-hidden="true"></i>
                    <i class="fa fa-caret-right fa-1" aria-hidden="true"></i>
                    <i class="fa fa-caret-right fa-1" aria-hidden="true"></i>
                </span>
                <section class="active_input form_flow_pc">
                    <p>２.ご入力内容のご確認</p>
                </section>
                <span class="formflow_arrows form_flow_pc">
                    <i class="fa fa-caret-right fa-1" aria-hidden="true"></i>
                    <i class="fa fa-caret-right fa-1" aria-hidden="true"></i>
                    <i class="fa fa-caret-right fa-1" aria-hidden="true"></i>
                </span>
                <section class="deact_input form_flow_pc">
                    <p>３.送信完了</p>
                </section>
            </div>

        <form name="form1" id="form1" class="btform1 confrm_pge" method="post" action="?">
            <input type="hidden" name="action" id="action" value="">
            <input type="hidden" name="return_flg" value="return">
EOM;
        foreach ($arrValue as $key => $value) {
            if ($key != 'action' and $key != 'return_flg') {
                echo "<input type=\"hidden\" name=\"{$key}\" value=\"{$value}\">\n";
            }
        }

        echo <<< EOM

                <div class="form_title">ご契約者様情報について</div>
                <table>
                    <tbody>
                        <tr>
                            <th>
                                <span>ご契約者名</span>
                            </th>
                            <td>
                                <div class="confirmation_value">{$arrValue["name"]}</div>
                            </td>
                        </tr>
                        <tr>
                            <th>
                                <span>ご契約者名(カナ）</span>
                            </th>
                            <td>
                                <div class="confirmation_value">{$arrValue["name_kana"]}</div>
                            </td>
                        </tr>
                        <tr>
                            <th>
                                <span>マンション名</span>
                            </th>
                            <td>
                                <div class="confirmation_value">{$arrValue["name_mansion"]}</div>
                            </td>
                        </tr>
                        <tr>
                            <th>
                                <span>お部屋番号</span>
                            </th>
                            <td>
                                <div class="confirmation_value">{$arrValue["room_number"]}</div>
                            </td>
                        </tr>
                        <tr valign="middle">
                            <th>
                                <span>メールアドレス</span>
                            </th>
                            <td>
                                <div class="confirmation_value">{$arrValue["Mail"]}</div>
                            </td>
                        </tr>
                    </tbody>
                </table>


                <div class="form_title">アンケート</div>
                <div class="ste_review">
                        <h3 class="">Q1.今回のリフォーム工事に対するご意見</h3>
                    <div class="question_list">
                        <div><span>弊社の提案内容 ：</span><span class="confirmation_value">{$arrValue["proposal_content"]}（{$proposal_number}）</span></div>
                        <div><span>担当者の対応 ：</span><span class="confirmation_value">{$arrValue["Personin_charge"]}（{$Personin_number}）</span></div>
                        <div><span>工事の出来栄え ：</span><span class="confirmation_value">{$arrValue["achievement"]}（{$achievement_number}）</span></div>
                        <div><span>現場作業員の対応 ：</span><span class="confirmation_value">{$arrValue["worker_response"]}（{$worker_number}）</span></div>
                        <div><span>施工管理／期日 ：</span><span class="confirmation_value">{$arrValue["due_date"]}（{$due_date_number}）</span></div>
                        <div><span>総合評価 ：</span><span class="confirmation_value">{$arrValue["evaluation"]}（{$evaluation_number}）</span></div>
                    </div>
                </div>

                <div class="ste_review">
                        <h3 class="">Q2.弊社にご発注いただいた決め手</h3>
                        <ul class="question_list_event_form">
                                <li><div class="confirmation_value">{$arrValue["AH_surv"]}</div></li>
EOM;
        if ($arrValue["AH7"] == 'その他：'){
        echo <<< EOM
            <li><div class="confirmation_value">その他：{$arrValue["AH7_by_others"]}</div></li>
EOM;
        }
        echo <<< EOM
                        </ul>
                        </div>
                
                <div class="ste_review">
                        <h3 class="">Q3.今後リフォームを検討する際、関心がある内容</h3>
                        <ul class="question_list_event_form">
                                <li><div class="confirmation_value">{$arrValue["BH_surv"]}</div></li>
EOM;
        if ($arrValue["BH6"] == 'その他：'){
        echo <<< EOM
            <li><div class="confirmation_value">その他：{$arrValue["BH6_by_others"]}</div></li>
EOM;
        }
        echo <<< EOM
                        </ul>
                        </div>
                
                <div class="ste_review">
                        <h3 class="">Q4.今後リフォームを検討したい部位（場所）</h3>
                        <ul class="question_list_event_form">
                                <li><div class="confirmation_value">{$arrValue["CH_surv"]}</div></li>
EOM;
        if ($arrValue["CH10"] == 'その他：'){
        echo <<< EOM
            <li><div class="confirmation_value">その他：{$arrValue["CH10_by_others"]}</div></li>
EOM;
        }
        echo <<< EOM
                        </ul>
                        </div>
                
                <div class="ste_review">
                        <h3 class="">Q5.次回のリフォームのご予定</h3>
                        <ul class="question_list_event_form">
                                <li><div class="confirmation_value">{$arrValue["DH_surv"]}</div></li>
EOM;
        if ($arrValue["DH8"] == 'その他：'){
        echo <<< EOM
            <li><div class="confirmation_value">その他：{$arrValue["DH8_by_others"]}</div></li>
EOM;
        }
        echo <<< EOM
                        </ul>
                        </div>
                
                <div class="ste_review">
                        <h3 class="">Q6.今後どのような情報の提供</h3>
                        <ul class="question_list_event_form">
                                <li><div class="confirmation_value">{$arrValue["EH_surv"]}</div></li>
EOM;
        if ($arrValue["EH6"] == 'その他：'){
        echo <<< EOM
            <li><div class="confirmation_value">その他：{$arrValue["EH6_by_others"]}</div></li>
EOM;
        }
        echo <<< EOM
                        </ul>
                        </div>
                
                <div class="ste_review">
                        <h3 class="">その他お気づきの点、ご要望・ご相談</h3>
                            <div class="confirmation_value section_note">{$note_value}</div>
                        </div>
                <br>
                

            <div class="form_title">個人情報の取り扱いについて</div>
                <table>
                <tbody>
                <tr>
                    <th><label>「個人情報の取り扱いについて」同意</label></th>
                    <td>
                        <div class="confirmation_value">{$arrValue["agree_check"]}</div>
                    </td>
                </tr>
                </tbody>
                </table>
                <br>


                    <br>
                        <p class="confrm_txt">入力内容をご確認いただき、宜しければ｢上記内容を送信する｣ボタンを押してください。</p><br><br>
                        <p class="button">
                        <input type="button" name="back" id="back" class="button_confirm_page button_backleft" onclick="fncFormSubmit('back')" value="戻る">
                        <input type="button" name="send" id="send" class="button_confirm_page button_backright" onclick="fncFormSubmit('send')" value="上記内容を送信する">
                        </p>
                        <br>


              <br>
            </form>

            <script src="./js/jquery-1.11.3.min.js"></script>
            <script>
                $(function() {
                    var elementClicked = false;
                    $(window).on("beforeunload", function() {
                        if ($('#action').val() != ''){
                            elementClicked = true;
                        }
                        if (!elementClicked) {
                            return "ページ移動を確認します";
                        }
                    });
                });
            </script>


            <div class="tag">
            <!-- Google Tag Manager -->
            <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WBCWL4"
            height="0" width="0"
            style="display:none;visibility:hidden"></iframe></noscript>
            <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-WBCWL4');</script>
            <!-- End Google Tag Manager -->

            <!-- Yahoo Code for your Target List -->
            <script type="text/javascript" language="javascript">
            /* <![CDATA[ */
            var yahoo_retargeting_id = 'DH6LU3GI2U';
            var yahoo_retargeting_label = '';
            var yahoo_retargeting_page_type = '';
            var yahoo_retargeting_items = [{item_id: '', category_id: '', price: '',
            quantity: ''}];
            /* ]]> */
            </script>
            <script type="text/javascript" language="javascript" src="https://b92.yahoo.
            co.jp/js/s_retargeting.js"></script>

            <!-- リマーケティング タグの Google コード -->
            <!--------------------------------------------------
            リマーケティング タグは、個人を特定できる情報と関連付けることも、デリケート
            なカテゴリに属するページに設置することも許可されません。タグの設定方法につい
            ては、こちらのページをご覧ください。
            http://google.com/ads/remarketingsetup
            --------------------------------------------------->
            <script type="text/javascript">
            /* <![CDATA[ */
            var google_conversion_id = 833749516;
            var google_custom_params = window.google_tag_params;
            var google_remarketing_only = true;
            /* ]]> */
            </script>
            <script type="text/javascript"
            src="//www.googleadservices.com/pagead/conversion.js">
            </script>
            <noscript>
            <div style="display:inline;">
            <img height="1" width="1" style="border-style:none;" alt=""
            src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/833749516/?g
            uid=ON&amp;script=0"/>
            </div>
            </noscript>

            </div>

EOM;
            // GoogleAnalyticsタグを表示
            displayHtmlGoogleAnalyticsTag('/form/contact/confirm.php');
    }




    // HTML送信完了画面を表示
    function displayHtmlComplete() {
        echo <<< EOM

        <div id="form_flow" class="form_flow frm_flw_end">
                <section class="deact_input form_flow_sp">
                    <p>１.<br>必要事項の<br>ご入力</p>
                </section>
                <span class="formflow_arrows form_flow_sp">
                    <i class="fa fa-caret-right fa-1" aria-hidden="true"></i>
                </span>
                <section class="deact_input form_flow_sp">
                    <p>２.<br>ご入力内容の<br>ご確認</p>
                </section>
                <span class="formflow_arrows form_flow_sp">
                    <i class="fa fa-caret-right fa-1" aria-hidden="true"></i>
                </span>
                <section class="active_input form_flow_sp">
                    <p>３.<br>送信完了</p>&nbsp;
                </section>
            </div>

            <div id="form_flow">
                <section class="deact_input form_flow_pc">
                    <p>１.必要事項のご入力</p>
                </section>
                <span class="formflow_arrows form_flow_pc">
                    <i class="fa fa-caret-right fa-1" aria-hidden="true"></i>
                    <i class="fa fa-caret-right fa-1" aria-hidden="true"></i>
                    <i class="fa fa-caret-right fa-1" aria-hidden="true"></i>
                </span>
                <section class="deact_input form_flow_pc">
                    <p>２.ご入力内容のご確認</p>
                </section>
                <span class="formflow_arrows form_flow_pc">
                    <i class="fa fa-caret-right fa-1" aria-hidden="true"></i>
                    <i class="fa fa-caret-right fa-1" aria-hidden="true"></i>
                    <i class="fa fa-caret-right fa-1" aria-hidden="true"></i>
                </span>
                <section class="active_input form_flow_pc">
                    <p>３.送信完了</p>
                </section>
            </div>



        <p class="ty_page" style="padding-top:50px;">
            この度は、東急Ｒｅ・デザインへご相談・お見積もり依頼を賜り、誠にありがとうございます。<br>
            <br>
            お問合せ内容につきまして、通常は3営業日以内にご連絡いたします。<br>
            万一、1週間以上たっても弊社担当者より連絡がない場合には、お手数ではございますが、<br>
            0120-994-109までご連絡をお願いいたします。<br>
            &nbsp;<br>
        </p>
        <br>
        <div id="bottom_text02">
            <a href="javascript:window.close();"><img src="images/close_btn.gif" alt="閉じる" /></a>
            <br><br>
            <a href="https://mr.tokyu-re-design.co.jp/">- TOPへ戻る -</a>
        </div>
        <br>
        <!--<div id="bottom_text03">
            <a href="https://www.tokyure-design.co.jp/">- TOPへ戻る -</a>
        </div>-->



        <div class="tag">
        <!-- Google Tag Manager -->
        <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WBCWL4"
        height="0" width="0"
        style="display:none;visibility:hidden"></iframe></noscript>
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-WBCWL4');</script>
        <!-- End Google Tag Manager -->

        <!-- Yahoo Code for your Target List -->
        <script type="text/javascript" language="javascript">
        /* <![CDATA[ */
        var yahoo_retargeting_id = 'DH6LU3GI2U';
        var yahoo_retargeting_label = '';
        var yahoo_retargeting_page_type = '';
        var yahoo_retargeting_items = [{item_id: '', category_id: '', price: '',
        quantity: ''}];
        /* ]]> */
        </script>
        <script type="text/javascript" language="javascript" src="https://b92.yahoo.
        co.jp/js/s_retargeting.js"></script>

        <!-- リマーケティング タグの Google コード -->
        <!--------------------------------------------------
        リマーケティング タグは、個人を特定できる情報と関連付けることも、デリケート
        なカテゴリに属するページに設置することも許可されません。タグの設定方法につい
        ては、こちらのページをご覧ください。
        http://google.com/ads/remarketingsetup
        --------------------------------------------------->
        <script type="text/javascript">
        /* <![CDATA[ */
        var google_conversion_id = 833749516;
        var google_custom_params = window.google_tag_params;
        var google_remarketing_only = true;
        /* ]]> */
        </script>
        <script type="text/javascript"
        src="//www.googleadservices.com/pagead/conversion.js">
        </script>
        <noscript>
        <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt=""
        src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/833749516/?g
        uid=ON&amp;script=0"/>
        </div>
        </noscript>

        </div>

EOM;

        // GoogleAnalyticsタグを表示
        displayHtmlGoogleAnalyticsTag('/form/contact/complete.php');

        echo <<< EOM


EOM;

    }
