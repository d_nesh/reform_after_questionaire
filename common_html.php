<?php

    // HTML共通ヘッダーを表示
    function displayHtmlHeader($title='') {
        echo <<< EOM
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1">
<title>リフォームアフターアンケート｜東急Ｒｅ・デザイン</title>
<meta name="description" content="東急Ｒｅ・デザインのマンションリフォーム" />
<meta name="keywords" content="東急Ｒｅ・デザイン,リフォーム,モデルハウス,見学会,住宅メーカー,ハウスメーカー" >

<!-- css -->
<link href="css/html5reset-1.6.1.css" rel="stylesheet">
<link href="css/main.css" rel="stylesheet">
<!-- //css -->

<link rel="stylesheet" href="./css/form.css">
<link rel="stylesheet" href="./font-awesome-4.7.0/css/font-awesome.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1/i18n/jquery.ui.datepicker-ja.min.js"></script>

<script type='text/javascript'>
jQuery(function($) {
  $.datepicker.setDefaults($.datepicker.regional['ja']);
  $("#reservation_first_option").datepicker();
});
</script>

<script type='text/javascript'>
jQuery(function($) {
  $.datepicker.setDefaults($.datepicker.regional['ja']);
  $("#reservation_second_option").datepicker();
});
</script>

<style>
    /* 土日を赤にする */
    .ui-datepicker-week-end, .ui-datepicker-week-end a.ui-state-default { color:red; }
</style>

<!-- 2019/01/16 Add -->
<!-- Global site tag (gtag.js) - Google Ads: 770043471 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-770043471"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-770043471');
</script>

<!-- Event snippet for 申し込み conversion page -->
<script>
  gtag('event', 'conversion', {'send_to': 'AW-770043471/ajS_CMSdlJMBEM_cl-8C'});
</script>

</head>
<body>

<!-- wrap -->
<div id="wrap">


<!--HEADER-->
<div id="lp_head">
    <!-- header -->
    <header>
        <div class="lp_head_inner">
                <div class="hd_row_top">
                    <div class="hd_column_lft">
                    <div class="lp_logo"><a href="https://mr.tokyu-re-design.co.jp/"><img src="./images/TRD_T-JPN_170814.gif" alt="東急Ｒｅ・デザインマンションリフォーム"></a></div>

                    </div>
                    <div class="hd_column_rght rght_algn">
                        <div class="lp_tel"><img src="images/top_tel.jpg" alt=""></div>
                    </div>
                    <p class="view-pc">暮らしをもっと「便利に」「素敵に」「楽しく」マンションリフォーム事業部</p>
                </div>
        </div>
        <div class="clear"></div>
    </header>
    <!-- //header -->
    <div class="clear"></div>
</div><!--HEADER-->

<div class="hd_main">
<p class="underBlock__title">リフォームアフターアンケート</p>

</div>


<!-- remove the commented out style line-height: 300px; in hd_main when removing goldenweek_txt -->


EOM;
    }



    // HTML共通フッターを表示
    function displayHtmlFooter() {
        echo <<< EOM
        </div>
    </div>
    </div>
    <script src="./js/form.js"></script>


    </main>
    <!-- //main -->

    <!--footer-->
<div id="common_footerContainer2">
	<div class="common_ft_c2_inner">
      <p class="common_copyright">© Tokyu Re・design Corporation All rights reserved.</p>
    </div>
</div>
    <!--//footer-->

    </div>
    <!-- //wrap -->


    <!--pagetop-->
    <p id="page-top"><a href="#top"><img src="images/pagetop.png"></a></p>
    <!--//pagetop-->


    <!-- js -->
    <!--<script src="js/jquery.min.js"></script>//-->
    <script src="js/jquery.easing.min.js"></script>
    <script src="js/web.js" ></script>
    <!-- //js -->



    <script type="text/javascript">
    $(function() {
        var topBtn = $('#page-top');
        topBtn.hide();
        //スクロールが100に達したらボタン表示
        $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
                topBtn.fadeIn();
            } else {
                topBtn.fadeOut();
            }
        });
        //スクロールしてトップ
        topBtn.click(function () {
            $('body,html').animate({
                scrollTop: 0
            }, 500);
            return false;
        });
    });
    </script>

    </body>

    <!-- 2019/01/16 -->
    <!-- Yahoo Code for your Conversion Page -->
    <script type="text/javascript">
        /* <![CDATA[ */
        var yahoo_conversion_id = 1001050158;
        var yahoo_conversion_label = "Jrr3COPW9ZIBEIWwse8C";
        var yahoo_conversion_value = 0;
        /* ]]> */
    </script>
    <script type="text/javascript" src="https://s.yimg.jp/images/listing/tool/cv/conversion.js">
    </script>
    <noscript>
        <div style="display:inline;">
            <img height="1" width="1" style="border-style:none;" alt="" src="https://b91.yahoo.co.jp/pagead/conversion/1001050158/?value=0&label=Jrr3COPW9ZIBEIWwse8C&guid=ON&script=0&disvt=true"/>
        </div>
    </noscript>

    </html>
EOM;
    }



    // HTMLサイドナビゲーションを表示
    function displayHtmlSideNavi() {
        echo <<< EOM
EOM;
    }


    // YYYY形式のselectboxを表示
    function makeOptionYear ($selected_value=''){
        // 今年を取得
        $option_year = date('Y');

        // 1900年から今年までoptionに設定する
        for ($i=$option_year; $i >= 1900; $i--) {
            if ($i == $selected_value) {
                $selected = 'selected';
            } else {
                $selected = '';
            }
            echo "<option value=\"$i\" $selected>$i</option>\n";
        }
    }

    // 1から引数までのselectboxを表示
    function makeOptionNumber ($limit, $selected_value=''){

        for ($i=1; $i <= $limit; $i++){
            if ($i == $selected_value) {
                $selected = 'selected';
            } else {
                $selected = '';
            }
            echo "<option value=\"$i\" $selected>$i</option>\n";
        }
    }


    // 配列の値からselectboxを表示
    function makeOptionText ($arrValue, $selected_value=''){

        foreach ($arrValue as $value) {
            if ($value == $selected_value) {
                $selected = 'selected';
            } else {
                $selected = '';
            }
            echo "<option value=\"$value\" $selected>$value</option>\n";
        }
    }
