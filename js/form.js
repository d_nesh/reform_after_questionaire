function fncFormSubmit(action_value) {
        document.getElementById("form1").action.value = action_value;
        document.getElementById("form1").submit();
}


function validateForm(){
    if ($(form1).validationEngine('validate')) {
        return true;
    }
    return false;
}


function fncTextValidate(element_id,label_name) {

    var required_label = "required_label_" + element_id;
    var ok_label = "ok_label_" + element_id;

    if (label_name != null) {
        required_label = "required_label_" + label_name;
        ok_label = "ok_label_" + label_name;
    }

    $('div').remove('.' + element_id + 'formError');

    if ($("#" + element_id).validationEngine('validate')) {
        if (document.getElementById(required_label) != null) {
            document.getElementById(required_label).style.display = "none";
            document.getElementById(ok_label).style.display = "inline-block";
        }
        document.getElementById(element_id).style.backgroundColor = "#FFFFFF";
        return true;
    } else {
        if (document.getElementById(required_label) != null) {
            document.getElementById(required_label).style.display = "inline-block";
            document.getElementById(ok_label).style.display = "none";
        }
        document.getElementById(element_id).style.backgroundColor = "#e7faff";
        return false;
    }
}


function fncCheckboxGroupValidate(arr_element_id,label_name,min_required) {

    var required_label = "required_label_" + label_name;
    var ok_label = "ok_label_" + label_name;

    var pass_count = 0;

    for (var i = 0; i < arr_element_id.length; i++) {
        if (document.getElementById(arr_element_id[i]).checked == true) {
            pass_count++;
        }
    }

    if (min_required == null) {
        min_required = 1;
    }

    if (pass_count >= min_required) {
        $("#"+arr_element_id[0]).removeClass("validate[required]");
        $('div').remove('.' + arr_element_id[0] + 'formError');
        document.getElementById(required_label).style.display = "none";
        document.getElementById(ok_label).style.display = "inline-block";
        return true;
    } else {
        $("#"+arr_element_id[0]).addClass("validate[required]");
        $("#"+arr_element_id[0]).validationEngine('validate');
        document.getElementById(required_label).style.display = "inline-block";
        document.getElementById(ok_label).style.display = "none";
        return false;
    }
}


function fncConvertToNarrow (element_id) {

    var convert_value = document.getElementById(element_id).value;

    if (convert_value.match(/[Ａ-Ｚａ-ｚ０-９－ー　＠]/)) {
        convert_value = convert_value.replace(/[Ａ-Ｚａ-ｚ０-９]/g,function(s){return String.fromCharCode(s.charCodeAt(0)-0xFEE0)});
        convert_value = convert_value.replace(/[－ー]/g,'-');
        convert_value = convert_value.replace(/[　]/g,' ');
        convert_value = convert_value.replace(/[＠]/g,'@');
        convert_value = convert_value.replace(/[．]/g,'.');
        document.getElementById(element_id).value = convert_value;
        return true;
    }

    return false;
}


function fncDisplayControl(check_id,control_class) {
    if (document.getElementById(check_id).checked == true) {
        $('.' + control_class).show();
        return true;
    }else{
        $('.' + control_class).hide();
        return false;
    }
}
