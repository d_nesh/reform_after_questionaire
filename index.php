<?php
header('X-FRAME-OPTIONS: SAMEORIGIN');
session_start();

    require_once('config.php');
    // メール本文設定ファイル
    require_once('mail_body.php');
    // htmlファイル
    require_once('html.php');
    // HTML共通データ
    require_once('common_html.php');
    // HTML共通データその２（アンケート他）
    require_once('common_html2.php');
    // メール送信処理関連
    require_once('./php_common/SC_Send_Mail.php');
    // CSV処理関連
    require_once('./php_common/Csv_Control.php');


    // HTML共通ヘッダーを表示
    displayHtmlHeader($title='');

    $mode = isset($_POST['action']) ? $_POST['action'] : '';

    switch ($mode) {

        // フォームの入力チェック
        case 'check':
            // tokenをチェック
            //lfCheckToken();

            // POSTの値をサニタイズ
            $arrForm_data = lfSanitizeFormData($_POST);

            // 必須項目の入力チェック
            $arrErr_msg = lfCheckInputValue($arrForm_data);


            // 入力内容にエラーがなければ確認画面を表示
            if (count($arrErr_msg) > 0) {
                $arrErr_msg['return_flg'] = true;
                displayHtmlInputForm($arrForm_data, $arrErr_msg);
            }else{
                // 確認画面を表示
                displayHtmlConfirmation($arrForm_data);
            }

            break;

        case 'send':

            // tokenをチェック
            //lfCheckToken();

            // POSTの値をサニタイズ
            $arrSend_data = lfSanitizeFormData($_POST);

            // 必須項目の入力チェック
            $arrErr_msg = lfCheckInputValue($arrSend_data);

            // 入力内容にエラーがあれば入力画面に戻す
            if (count($arrErr_msg) > 0) {
                 displayHtmlInputForm($arrSend_data, $arrErr_msg);
                 break;
            }

            // 仕様に合わせて入力データを変換
            $arrSend_data = lfArrangeFormData($arrSend_data);

            $str = $arrSend_data['Zip'];
            if(preg_match('/^[0-9]{3}-[0-9]{4}$/', $str)) {
            //  echo "123-1234";
            }else{
            //    echo "1231234 : ";
                $edtZip = preg_replace('/^(\d{3})(\d{4})$/', '$1-$2', $str);
                $arrSend_data['Zip'] = $edtZip;
            }

            $str = $arrSend_data['Zip2'];
            if(preg_match('/^[0-9]{3}-[0-9]{4}$/', $str)) {
            //  echo "123-1234";
            }else{
            //    echo "1231234 : ";
                $edtZip = preg_replace('/^(\d{3})(\d{4})$/', '$1-$2', $str);
                $arrSend_data['Zip2'] = $edtZip;
            }

            // CSV書き込み処理
            lfWritingCSV($arrSend_data);

            // 管理者用メールの件名を設定
            $mail_subject = MAIL_SUBJECT_ADMIN;

            // 管理者用メール本文を取得
            $mail_body_admin = makeMailBodyAdmin($arrSend_data);

            // 管理者にメールを送信
            if( SC_Send_Mail::create()
                ->from( ADMIN_MAIL_FROM )
                ->to( ADMIN_MAIL_TO )
                ->cc( array( ADMIN_MAIL_CC1 ) )
                ->title( $mail_subject )
                ->body( $mail_body_admin )
                ->send()
                ){
            }


            // 申込者用メールの件名を設定
            $mail_subject = MAIL_SUBJECT_CUSTOMER;

            // 申込者用メール本文を取得
            $mail_body_customer = makeMailBodyUser($arrSend_data);

            // 申込者宛にメールを送信
            if( SC_Send_Mail::create()
                ->from( ADMIN_MAIL_FROM )
                ->to( $arrSend_data['Mail'] )
                ->title( $mail_subject )
                ->body( $mail_body_customer )
                ->send()
                ){
            }

            unset($_SESSION['token']);

            // 完了画面表示
            displayHtmlComplete();
            break;

        default:
            // CSRF対策 - sessionにtokenをセット
            lfSetToken();

            // POSTの値をサニタイズ
            $arrForm_data = lfSanitizeFormData($_POST);

            // HTML入力フォームを表示
            displayHtmlInputForm($arrForm_data);

            break;
    }

    // HTML共通フッターを表示
    displayHtmlFooter();




    // フォーム内容の入力チェック
    function lfCheckInputValue($arrValue) {

        $arr_error = array();

        if ( !$arrValue['name'] ) {
            $arr_error['name'] = 'お名前が入力されていません';
        }
        if ( !$arrValue['name_kana'] ) {
            $arr_error['name_kana'] = 'お名前(カナ)が入力されていません';
        }
        if ( !$arrValue['name_mansion'] ) {
            $arr_error['name_mansion'] = 'マンション名が入力されていません';
        }
        if ( !$arrValue['room_number'] ) {
            $arr_error['room_number'] = 'お部屋番号が入力されていません';
        }
        if ( !$arrValue['Mail'] ) {
            $arr_error['Mail'] = 'メールアドレスが入力されていません';
            // メールアドレスが正しいか確認
        } elseif ( !filter_var($arrValue['Mail'], FILTER_VALIDATE_EMAIL) ) {
            $arr_error['Mail'] = 'メールアドレスが正しく入力されていません';
        }

        return $arr_error;
    }


    // 入力された内容のサニタイズ
    function lfSanitizeFormData($arrValue) {

        foreach ($arrValue as $key => $value) {
            $arrValue[$key] = strip_tags($arrValue[$key]);
            $arrValue[$key] = htmlspecialchars($arrValue[$key], ENT_QUOTES);
            $arrValue[$key] = trim($arrValue[$key]);
        }

        return $arrValue;
    }


    // 仕様に合わせて入力されたデータを整える
    function lfArrangeFormData ($arrValue) {
        $arrValue['Zip'] = mb_convert_kana($arrValue['Zip'], 'a', "utf-8");
        $arrValue['Tel'] = mb_convert_kana($arrValue['Tel'], 'a', "utf-8");
        return $arrValue;
    }


    function lfSetToken() {
        unset($_SESSION['token']);
        $token = sha1(uniqid(mt_rand(), true));
        $_SESSION['token'] = $token;
    }


    function lfCheckToken() {
        if (empty($_SESSION['token']) || ($_SESSION['token'] != $_POST['token'])) {
            // 入力フォームに戻す
            $_POST = null;
            lfSetToken();
            $arrErr_msg['token_err'] = '接続がリセットされました。';
            displayHtmlInputForm(array(),$arrErr_msg);
            displayHtmlFooter();
            exit;
        }
    }



    function lfWritingCSV($arrData) {
        // CSV書き込み用にデータを配列にまとめる

        $proposal_number ='';
        if (isset($arrData['proposal_content'])) {
            switch ($arrData['proposal_content']) {
                case '満足':
                    $proposal_number = '5';
                    break;
                case 'ほぼ満足':
                    $proposal_number = '4';
                    break;
                case '普通':
                    $proposal_number = '3';
                    break;
                case 'やや不満':
                    $proposal_number = '2';
                    break;
                case '不満':
                    $proposal_number = '1';
                    break;
            }
        }

        $Personin_number ='';
        if (isset($arrData['Personin_charge'])) {
            switch ($arrData['Personin_charge']) {
                case '満足':
                    $Personin_number = '5';
                    break;
                case 'ほぼ満足':
                    $Personin_number = '4';
                    break;
                case '普通':
                    $Personin_number = '3';
                    break;
                case 'やや不満':
                    $Personin_number = '2';
                    break;
                case '不満':
                    $Personin_number = '1';
                    break;
            }
        }

        $achievement_number ='';
        if (isset($arrData['achievement'])) {
            switch ($arrData['achievement']) {
                case '満足':
                    $achievement_number = '5';
                    break;
                case 'ほぼ満足':
                    $achievement_number = '4';
                    break;
                case '普通':
                    $achievement_number = '3';
                    break;
                case 'やや不満':
                    $achievement_number = '2';
                    break;
                case '不満':
                    $achievement_number = '1';
                    break;
            }
        }

        $worker_number ='';
        if (isset($arrData['worker_response'])) {
            switch ($arrData['worker_response']) {
                case '満足':
                    $worker_number = '5';
                    break;
                case 'ほぼ満足':
                    $worker_number = '4';
                    break;
                case '普通':
                    $worker_number = '3';
                    break;
                case 'やや不満':
                    $worker_number = '2';
                    break;
                case '不満':
                    $worker_number = '1';
                    break;
            }
        }

        $due_date_number ='';
        if (isset($arrData['due_date'])) {
            switch ($arrData['due_date']) {
                case '満足':
                    $due_date_number = '5';
                    break;
                case 'ほぼ満足':
                    $due_date_number = '4';
                    break;
                case '普通':
                    $due_date_number = '3';
                    break;
                case 'やや不満':
                    $due_date_number = '2';
                    break;
                case '不満':
                    $due_date_number = '1';
                    break;
            }
        }

        $evaluation_number ='';
        if (isset($arrData['evaluation'])) {
            switch ($arrData['evaluation']) {
                case '満足':
                    $evaluation_number = '5';
                    break;
                case 'ほぼ満足':
                    $evaluation_number = '4';
                    break;
                case '普通':
                    $evaluation_number = '3';
                    break;
                case 'やや不満':
                    $evaluation_number = '2';
                    break;
                case '不満':
                    $evaluation_number = '1';
                    break;
            }
        }

        $ah_list_surv = '';

        for ($i=1; $i < 7; $i++) {
            if (isset($arrData["AH$i"])){
                $ah_list_surv .= $arrData["AH$i"] ."/";
            }
        }

        if (isset($arrData["AH7"])){
            $ah_list_surv .= $arrData["AH7"];
                $ah_list_surv .= $arrData["AH7_by_others"];
        }

        $bh_list_surv = '';

        for ($i=1; $i < 6; $i++) {
            if (isset($arrData["BH$i"])){
                $bh_list_surv .= $arrData["BH$i"] ."/";
            }
        }

        if (isset($arrData["BH6"])){
            $bh_list_surv .= $arrData["BH6"];
                $bh_list_surv .= $arrData["BH6_by_others"];
        }

        $ch_list_surv = '';

        for ($i=1; $i < 10; $i++) {
            if (isset($arrData["CH$i"])){
                $ch_list_surv .= $arrData["CH$i"] ."/";
            }
        }

        if (isset($arrData["CH10"])){
            $ch_list_surv .= $arrData["CH10"];
                $ch_list_surv .= $arrData["CH10_by_others"];
        }

        $dh_list_surv = '';

        for ($i=1; $i < 8; $i++) {
            if (isset($arrData["DH$i"])){
                $dh_list_surv .= $arrData["DH$i"] ."/";
            }
        }

        if (isset($arrData["DH8"])){
            $dh_list_surv .= $arrData["DH8"];
                $dh_list_surv .= $arrData["DH8_by_others"];
        }

        $eh_list_surv = '';

        for ($i=1; $i < 6; $i++) {
            if (isset($arrData["EH$i"])){
                $eh_list_surv .= $arrData["EH$i"] ."/";
            }
        }

        if (isset($arrData["EH6"])){
            $eh_list_surv .= $arrData["EH6"];
                $eh_list_surv .= $arrData["EH6_by_others"];
        }

        $arrCsv_data = array();
        $arrCsv_data[] = isset($arrData['name']) ? $arrData['name'] : '';
        $arrCsv_data[] = isset($arrData['name_kana']) ? $arrData['name_kana'] : '';
        $arrCsv_data[] = isset($arrData['name_mansion']) ? $arrData['name_mansion'] : '';
        $arrCsv_data[] = isset($arrData['room_number']) ? $arrData['room_number'] : '';
        $arrCsv_data[] = isset($arrData['Mail']) ? $arrData['Mail'] : '';
        $arrCsv_data[] = isset($proposal_number) ? $proposal_number : '';
        $arrCsv_data[] = isset($Personin_number) ? $Personin_number : '';
        $arrCsv_data[] = isset($achievement_number) ? $achievement_number : '';
        $arrCsv_data[] = isset($worker_number) ? $worker_number : '';
        $arrCsv_data[] = isset($due_date_number) ? $due_date_number : '';
        $arrCsv_data[] = isset($evaluation_number) ? $evaluation_number : '';
        $arrCsv_data[] = isset($ah_list_surv) ? $ah_list_surv : '';
        $arrCsv_data[] = isset($bh_list_surv) ? $bh_list_surv : '';
        $arrCsv_data[] = isset($ch_list_surv) ? $ch_list_surv : '';
        $arrCsv_data[] = isset($dh_list_surv) ? $dh_list_surv : '';
        $arrCsv_data[] = isset($eh_list_surv) ? $eh_list_surv : '';
        $arrCsv_data[] = isset($arrData['Note']) ? $arrData['Note'] : '';

        $arrCsv_data[] = date("YmdHis");

        // CSV用にデータ整形
        $arrCsv_data = sfTrimDataForCSV($arrCsv_data);

        // CSV書き込み処理
        sfWriteCSV (CSV_FILE, $arrCsv_data);
    }
