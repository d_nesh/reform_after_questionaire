<?php

    /**
     * CSV書き込み処理
     *
     * @param $file_path：書き込むCSVファイルのパス（呼び出し元から見た位置）
     * @param $arrData：CSVへ書き込むデータ。全ての要素が1レコードで書き込まれる
     * @return
     */
    function sfWriteCSV ($file_path, $arrData ) {

        $csv_data = '';

        // 要素数を確認
        $count = count($arrData);

        for ($i = 0; $i < $count; $i++){
            $csv_data .= "$arrData[$i],";
        }

        // 最後のカンマを削除
        $csv_data = substr($csv_data, 0, -1);

        $csv_data .= "\n";

        $csv_data = mb_convert_encoding($csv_data, 'SJIS-win','UTF-8');

        $CSVfp = fopen($file_path, 'a');
        fwrite($CSVfp, $csv_data);
        fclose($CSVfp);

    }


    /**
     * CSV用のデータ整形処理
     * 改行を削除し、カンマを句読点に変換する
     *
     * @param $csv_data：整形するデータ
     * @return $trim_data：整形されたデータ
     */
    function sfTrimDataForCSV($csv_data){

        $trim_data = $csv_data;

        // 改行を削除
        $trim_data = preg_replace('/(?:\n|\r|\r\n)/', '', $trim_data );

        // カンマを「、」に置換
        $trim_data = str_replace(',', '、', $trim_data);
        $trim_data = str_replace('，', '、', $trim_data);

        return $trim_data;

    }
