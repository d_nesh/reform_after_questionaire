<?php

    // アンケート項目を表示
    function displayHtmlQuestionnaire($trigger_etc='',$trigger_welbox='') {

        for ($i=0; $i < 26; $i++){
            $checked_Trigger_radio[$i] = '';
        }
        if (isset($_POST['Trigger_radio'])) {
            switch ($_POST['Trigger_radio']) {
                case '読売新聞':
                    $checked_Trigger_radio1 = 'checked';
                    break;
                case '朝日新聞':
                    $checked_Trigger_radio2 = 'checked';
                    break;
                case 'チラシ':
                    $checked_Trigger_radio3 = 'checked';
                    break;
                case '交通広告':
                    $checked_Trigger_radio4 = 'checked';
                    break;
                case '東急ホームズのウェブサイト':
                    $checked_Trigger_radio5 = 'checked';
                    break;
                case 'Goodリフォーム.jp':
                    $checked_Trigger_radio6 = 'checked';
                    break;
                case 'SUUMOリフォーム':
                    $checked_Trigger_radio7 = 'checked';
                    break;
                case 'その他ウェブサイト':
                    $checked_Trigger_radio8 = 'checked';
                    break;
                case 'SUUMOリフォーム（Goodリフォーム）':
                    $checked_Trigger_radio9 = 'checked';
                    break;
                case 'SUUMOリフォーム（実例＆会社が見つかる本）':
                    $checked_Trigger_radio10 = 'checked';
                    break;
                case '中古を買ってリノベーション':
                    $checked_Trigger_radio11 = 'checked';
                    break;
                case 'その他雑誌':
                    $checked_Trigger_radio12 = 'checked';
                    break;
                case 'こすもす':
                    $checked_Trigger_radio13 = 'checked';
                    break;
                case 'リビング新聞':
                    $checked_Trigger_radio14 = 'checked';
                    break;
                case '弊社でリフォームされたお客様から':
                    $checked_Trigger_radio15 = 'checked';
                    break;
                case '東急関連会社社員から':
                    $checked_Trigger_radio16 = 'checked';
                    break;
                case '弊社社員から':
                    $checked_Trigger_radio17 = 'checked';
                    break;
                case '工事現場看板':
                    $checked_Trigger_radio18 = 'checked';
                    break;
                case 'ショールーム':
                    $checked_Trigger_radio19 = 'checked';
                    break;
                case 'イベント':
                    $checked_Trigger_radio20 = 'checked';
                    break;
                case 'その他':
                    $checked_Trigger_radio21 = 'checked';
                    break;
                case 'LIMIA（リミア）':
                    $checked_Trigger_radio22 = 'checked';
                    break;
                case 'WELBox':
                    $checked_Trigger_radio23 = 'checked';
                    break;
                case 'その他の新聞':
                    $checked_Trigger_radio24 = 'checked';
                    break;
                case 'SUVACO':
                    $checked_Trigger_radio25 = 'checked';
                    break;
                case 'その他フリーペーパー':
                    $checked_Trigger_radio26 = 'checked';
                    break;
            }
        }

        echo <<< EOM
        <br />

        <h3 class="gtitle">
            <span class="label label-danger required_label" id="required_label_Trigger_radio">必須</span>
            <span class="label label-success ok_label" id="ok_label_Trigger_radio">ＯＫ</span>
            <label for="Trigger_radio">弊社をお知りになったきっかけについて（以下から一つ選択してください。）</label>
        </h3>

        <table cellpadding="0" cellspacing="0" summary="" class="form_table" style="border:none;">
            <tr>
                <td style="border:none;">
                    <span class="error_text">{$arrErrorText["Trigger_radio"]}</span>

                    <strong>■ 広告：</strong><br /> &nbsp;&nbsp;
                    <input type="radio"
                        name="Trigger_radio"
                        id="Trigger_radio3"
                        {$checked_Trigger_radio3}
                        value="チラシ"
                        style="vertical-align:middle;"
                        class="validate[required]"
                        data-prompt-position="topRight"/>
                    <label for="Trigger_radio3">チラシ</label>
                    <input type="radio"
                        name="Trigger_radio"
                        id="Trigger_radio1"
                        {$checked_Trigger_radio1}
                        value="読売新聞"
                        style="vertical-align:middle;"
                        class="validate[required]"/>
                    <label for="Trigger_radio1">読売新聞</label>
                    <input type="radio"
                        name="Trigger_radio"
                        id="Trigger_radio2"
                        {$checked_Trigger_radio2}
                        value="朝日新聞"
                        style="vertical-align:middle;"
                        class="validate[required]"/>
                    <label for="Trigger_radio2">朝日新聞</label>

                    <input type="radio"
                        name="Trigger_radio"
                        id="Trigger_radio24"
                        {$checked_Trigger_radio24}
                        value="その他の新聞"
                        style="vertical-align:middle;"
                        class="validate[required]"/>
                    <label for="Trigger_radio24">その他の新聞</label>

                    <input type="radio"
                        name="Trigger_radio"
                        id="Trigger_radio4"
                        {$checked_Trigger_radio4}
                        value="交通広告"
                        style="vertical-align:middle;"
                        class="validate[required]"/>
                    <label for="Trigger_radio4">交通広告</label>
                    <br />
                    <strong>■ インターネット：</strong><br /> &nbsp;&nbsp;
                    <input type="radio"
                        name="Trigger_radio"
                        id="Trigger_radio5"
                        {$checked_Trigger_radio5}
                        value="東急ホームズのウェブサイト"
                        style="vertical-align:middle;"
                        class="validate[required]"/>
                    <label for="Trigger_radio5">東急ホームズのウェブサイト</label>
                    <input type="radio"
                        name="Trigger_radio"
                        id="Trigger_radio6"
                        {$checked_Trigger_radio6}
                        value="Goodリフォーム.jp"
                        style="vertical-align:middle;"
                        class="validate[required]"/>
                    <label for="Trigger_radio6">Goodリフォーム.jp</label>
                    <input type="radio"
                        name="Trigger_radio"
                        id="Trigger_radio7"
                        {$checked_Trigger_radio7}
                        value="SUUMOリフォーム（WEB）"
                        style="vertical-align:middle;"
                        class="validate[required]"/>
                    <label for="Trigger_radio7">SUUMOリフォーム（WEB）</label>
                    <input type="radio"
                        name="Trigger_radio"
                        id="Trigger_radio22"
                        {$checked_Trigger_radio22}
                        value="LIMIA（リミア）"
                        style="vertical-align:middle;"
                        class="validate[required]"/>
                    <label for="Trigger_radio22">LIMIA（リミア）</label>
                    <input type="radio"
                        name="Trigger_radio"
                        id="Trigger_radio25"
                        {$checked_Trigger_radio25}
                        value="SUVACO"
                        style="vertical-align:middle;"
                        class="validate[required]"/>
                    <label for="Trigger_radio25">SUVACO</label>
                    <br /> &nbsp;&nbsp;
                    <input type="radio"
                        name="Trigger_radio"
                        id="Trigger_radio23"
                        {$checked_Trigger_radio23}
                        value="WELBox"
                        style="vertical-align:middle;"
                        class="validate[required]"/>
                    <label for="Trigger_radio23">WELBox</label>（会員番号
                    <input type="text" name="Trigger_welbox" id="Trigger_welbox"
                        class="small_text"
                        value="{$trigger_welbox}"
                        placeholder=""/>）
                    <input type="radio"
                        name="Trigger_radio"
                        id="Trigger_radio8"
                        {$checked_Trigger_radio8}
                        value="その他ウェブサイト"
                        style="vertical-align:middle;"
                        class="validate[required]"/>
                    <label for="Trigger_radio8">その他ウェブサイト</label>
                    <br />
                    <strong>■ 雑誌：</strong><br /> &nbsp;&nbsp;
                    <input type="radio"
                        name="Trigger_radio"
                        id="Trigger_radio9"
                        {$checked_Trigger_radio9}
                        value="SUUMOリフォーム（Goodリフォーム）"
                        style="vertical-align:middle;"
                        class="validate[required]"/>
                    <label for="Trigger_radio9">SUUMOリフォーム（Goodリフォーム）</label>
                    <input type="radio"
                        name="Trigger_radio"
                        id="Trigger_radio10"
                        {$checked_Trigger_radio10}
                        value="SUUMOリフォーム（実例＆会社が見つかる本）"
                        style="vertical-align:middle;"
                        class="validate[required]"/>
                    <label for="Trigger_radio10">SUUMOリフォーム（実例＆会社が見つかる本）</label>
                    <input type="radio"
                        name="Trigger_radio"
                        id="Trigger_radio11"
                        {$checked_Trigger_radio11}
                        value="中古を買ってリノベーション"
                        style="vertical-align:middle;"
                        class="validate[required]"/>
                    <label for="Trigger_radio11">中古を買ってリノベーション</label>
<!--
                    <input type="radio"
                        name="Trigger_radio"
                        id="Trigger_radio12"
                        {$checked_Trigger_radio12}
                        value="その他雑誌"
                        style="vertical-align:middle;"
                        class="validate[required]"/>
                    <label for="Trigger_radio12">その他雑誌</label>
//-->
                    <br />
                    <strong>■ フリーペーパー：</strong><br /> &nbsp;&nbsp;
                    <input type="radio"
                        name="Trigger_radio"
                        id="Trigger_radio13"
                        {$checked_Trigger_radio13}
                        value="こすもす"
                        style="vertical-align:middle;"
                        class="validate[required]"/>
                    <label for="Trigger_radio13">こすもす</label>
                    <input type="radio"
                        name="Trigger_radio"
                        id="Trigger_radio14"
                        {$checked_Trigger_radio14}
                        value="リビング新聞"
                        style="vertical-align:middle;"
                        class="validate[required]"/>
                    <label for="Trigger_radio14">リビング新聞</label>
                    <input type="radio"
                        name="Trigger_radio"
                        id="Trigger_radio26"
                        {$checked_Trigger_radio26}
                        value="その他フリーペーパー"
                        style="vertical-align:middle;"
                        class="validate[required]"/>
                    <label for="Trigger_radio26">その他フリーペーパー</label>
                    <br />
                    <strong>■ 紹介：</strong><br /> &nbsp;&nbsp;
                    <input type="radio"
                        name="Trigger_radio"
                        id="Trigger_radio15"
                        {$checked_Trigger_radio15}
                        value="弊社でリフォームされたお客様から"
                        style="vertical-align:middle;"
                        class="validate[required]"/>
                    <label for="Trigger_radio15">弊社でリフォームされたお客様から</label>
                    <input type="radio"
                        name="Trigger_radio"
                        id="Trigger_radio16"
                        {$checked_Trigger_radio16}
                        value="東急関連会社社員から"
                        style="vertical-align:middle;"
                        class="validate[required]"/>
                    <label for="Trigger_radio16">東急関連会社社員から</label>
                    <input type="radio"
                        name="Trigger_radio"
                        id="Trigger_radio17"
                        {$checked_Trigger_radio17}
                        value="弊社社員から"
                        style="vertical-align:middle;"
                        class="validate[required]"/>
                    <label for="Trigger_radio17">弊社社員から</label>
                    <br />
                    <strong>■ その他：</strong><br /> &nbsp;&nbsp;
                    <input type="radio"
                        name="Trigger_radio"
                        id="Trigger_radio18"
                        {$checked_Trigger_radio18}
                        value="工事現場看板"
                        style="vertical-align:middle;"
                        class="validate[required]"/>
                    <label for="Trigger_radio18">工事現場看板</label>
                    <input type="radio"
                        name="Trigger_radio"
                        id="Trigger_radio19"
                        {$checked_Trigger_radio19}
                        value="ショールーム"
                        style="vertical-align:middle;"
                        class="validate[required]"/>
                    <label for="Trigger_radio19">ショールーム</label>
                    <input type="radio"
                        name="Trigger_radio"
                        id="Trigger_radio20"
                        {$checked_Trigger_radio20}
                        value="イベント"
                        style="vertical-align:middle;"
                        class="validate[required]"/>
                    <label for="Trigger_radio20">イベント</label>
                    <input type="radio"
                        name="Trigger_radio"
                        id="Trigger_radio21"
                        {$checked_Trigger_radio21}
                        value="その他"
                        style="vertical-align:middle;"
                        class="validate[required]"/>
                    <label for="Trigger_radio21">その他</label>
                    <input type="text" name="Trigger_etc" id="Trigger_etc"
                        class="middle_text"
                        value="{$trigger_etc}"
                        placeholder=""/>
                </td>
            </tr>
        </table>
        <br />
EOM;
    }


    function displayHtmlQuestionnaireJs() {
        echo <<< EOM
            $('input[name="Trigger_radio"]:radio').change(function() {
                fncControlTrigger();
            });

            function fncControlTrigger() {
                value = $('input[name=Trigger_radio]:checked').val();

                if (value) {
                    $("#required_label_Trigger_radio").hide();
                    $("#ok_label_Trigger_radio").show();
                }else{
                    $("#required_label_Trigger_radio").show();
                    $("#ok_label_Trigger_radio").hide();
                }

                if (value == "WELBox") {
                    $("#Trigger_welbox").prop('disabled', false);
                }else{
                    $("#Trigger_welbox").prop('disabled', true);
                }

                if (value == "その他") {
                    $("#Trigger_etc").prop('disabled', false);
                }else{
                    $("#Trigger_etc").prop('disabled', true);
                }
            }
EOM;
        }


    // GoogleAnalyticsタグを表示
    function displayHtmlGoogleAnalyticsTag($url) {
        echo <<< EOM

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async
        src="https://www.googletagmanager.com/gtag/js?id=UA-10856806-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());
          gtag('config', 'UA-10856806-1', {
            'page_path': '{$url}'
            });
        </script>

EOM;
        }

        // GoogleAnalyticsタグを表示
    function displayHtmlGoogleAnalyticsTag_GA($url) {
        echo <<< EOM

        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
            ga('create', 'UA-24158997-1', 'auto');
            ga('send', 'pageview', '{$url}');
        </script>

EOM;
        }
