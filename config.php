
<?php

    // メールの送信元アドレス
    define('ADMIN_MAIL_FROM','trdreform@tokyu-rd.jp');
    //define('ADMIN_MAIL_FROM','info@bandt.co.jp');

    // 管理者宛メールの送信先
    //define('ADMIN_MAIL_TO','ayano-kondo@tokyu-rd.jp');
    define('ADMIN_MAIL_TO','nadundinesha@bandt.co.jp');

    // 管理者宛メールの送信先:CC
    //define('ADMIN_MAIL_CC1','kaori-yoneya@tokyu-rd.jp');
    //define('ADMIN_MAIL_CC1','eriko-imanaga@tokyu-rd.jp');
    //define('ADMIN_MAIL_CC1','info@bandt.co.jp');
    define('ADMIN_MAIL_CC1','bt_tst001@bandt.co.jp');

    // メールタイトル：管理者用
    define('MAIL_SUBJECT_ADMIN','【東急Ｒｅ・デザイン マンションリフォーム】リフォームアフターご相談がありました');

    // メールタイトル：申込者用
    define('MAIL_SUBJECT_CUSTOMER','【東急Ｒｅ・デザイン マンションリフォーム】リフォームアフターご相談ありがとうございます');

    // CSVファイル位置
    define('CSV_FILE', __DIR__ .'/csv_data/reform_after.csv');
